﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using SkatSharp.DB;

namespace SkatSharp
{
	/// <summary>
	/// Hauptklasse für das Spiel
    /// Managt die Spieler sowie den allgemeinen Spielablauf
	/// </summary>
    public class Spiel
    {
        #region Private Variablen
        private RichTextBox rtb;
        private MenschenSpieler mensch;
        private KISpieler bot_0, bot_1, bot_2;
    	private List<Spieler> spieler;
    	private Spieler alleinSpieler;       
    	
        private Spieler aufspieler;

        private Spieler vorhand;
        private Spieler mittelhand;
        private Spieler hinterhand;
        private Spieler gewinner;

        
        private int spielwert;
        private bool verloren = false;
        private bool nullspiel = false;
        private bool ueberreizt = false;
        internal static bool running = false;

        private int runde;
        private int solopunkte;
    	
    	private List<Spielkarte> kartenStapel;
        private List<Spielkarte> stich;
        private Spielkarte besteKarte;
        private List<Spielkarte> skat;

        private Action reizAction;
        private Action<Spielart,Spielmodus, Kartenfarbe> taufeAction;
        private Action<Spieler, Spielkarte> proceedDel;

        private Panel panelMensch;
        private Panel panelKI1;
        private Panel panelKI2;
        private Panel panelStiche;
        private Graphics gc;

        private bool ausgewertet;
        private int falschauswerten = 0;

        private Action<Spieler> neuesSpielAction;
        
        private BackgroundWorker backgroundworkerStich;

        private bool ausgepasst = false;

        #endregion

        #region Statische Variablen
        private static Kartenfarbe trumpf;
        private static Spielart spielart;
        private static Spielmodus spielmodus;
        public static int aktuellerReizwert;
        public static Spielart AktuelleSpielart
        {
            get
            {
                return spielart;
            }
        }
        public static Spielmodus AktuellerSpielmodus
        {
            get
            {
                return spielmodus;
            }
        }
        public static Kartenfarbe Trumpf
        {
            get
            {
                return trumpf;
            }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Konstruktor des Spiels
        /// Benötigt die Panels der Spieler zum Anzeigen,
        /// den Delegaten für neue Spiele sowie eine
        /// Richtextbox für die Konsolenanzeige (Debug)
        /// </summary>
        /// <param name="panelStiche"></param>
        /// <param name="panelMensch"></param>
        /// <param name="panelKI1"></param>
        /// <param name="panelKI2"></param>
        /// <param name="rtb"></param>
        /// <param name="neuesSpielAction"></param>
        public Spiel(
            Panel panelStiche,
            Panel panelMensch,
            Panel panelKI1,
            Panel panelKI2,
            RichTextBox rtb,
            Action<Spieler> neuesSpielAction)
        {
            this.neuesSpielAction = neuesSpielAction;
            this.rtb = rtb;
            this.panelStiche = panelStiche;
            gc = panelStiche.CreateGraphics();
            this.panelStiche.Paint += panelStiche_Paint;

            this.panelMensch = panelMensch;
            this.panelKI1 = panelKI1;
            this.panelKI2 = panelKI2;

            kartenStapel = new List<Spielkarte>();

            if (proceedDel == null)
            {
                proceedDel += proceed;
            }
            

            backgroundworkerStich = new BackgroundWorker();
            backgroundworkerStich.DoWork += backgroundworkerStich_DoWork;
        }
        #endregion

        #region Reizen
        /// <summary>
        /// Managt das Reizen durch rekursive Aufrufe
        /// Ruft nach Abschluss taufe() auf
        /// </summary>
        private void weiterreizen()
        {
            if (alleinSpieler != null)
            {
                taufe();
            }
            else
            {
                if (aktuellerReizwert == -1)
                {
                    aktuellerReizwert = 18;
                }
                else
                {
                    aktuellerReizwert = Reizen.getNextIntReizwert(aktuellerReizwert);
                }
                if (mittelhand.spielwillig)
                {
                    if (vorhand.spielwillig)
                    {
                        mittelhand.fragen(vorhand);
                    }
                    else if (hinterhand.spielwillig)
                    {
                        hinterhand.fragen(mittelhand);
                    }
                    else
                    {                        
                        alleinSpieler = mittelhand;
                        if (alleinSpieler.GetType() == typeof(MenschenSpieler))
                        {
                            if (((MenschenSpieler)alleinSpieler).letzteChance())
                            {
                                taufe();
                            }
                            else
                            {
                                ausgepasst = true;
                                auswertung();
                            }
                        }
                        else
                        {
                            taufe();
                        }
                        
                    }
                }
                else if (vorhand.spielwillig)
                {
                    if (hinterhand.spielwillig)
                    {
                        hinterhand.fragen(vorhand);
                    }
                    else
                    {
                        alleinSpieler = vorhand;
                        if (alleinSpieler.GetType() == typeof(MenschenSpieler))
                        {
                            if (((MenschenSpieler)alleinSpieler).letzteChance())
                            {
                                taufe();
                            }
                            else
                            {
                                ausgepasst = true;
                                auswertung();
                            }
                        }
                        else
                        {
                            taufe();
                        }
                    }
                }
                else
                {
                    if (hinterhand.spielwillig)
                    {
                        alleinSpieler = hinterhand;
                        if (alleinSpieler.GetType() == typeof(MenschenSpieler))
                        {
                            if (((MenschenSpieler)alleinSpieler).letzteChance())
                            {
                                taufe();
                            }
                            else
                            {
                                ausgepasst = true;
                                auswertung();
                            }
                        }
                        else
                        {
                            taufe();
                        }
                    }
                    else
                    {
                        ausgepasst = true;
                        auswertung();
                    }
                }


            }
        }
        #endregion

        #region Spielvorbereitung
        /// <summary>
        /// Vorbereitungsmethode
        /// Spieler und Karten werden erstellt
        /// </summary>
        public void setUp()
        {
            // Vorbereitungen
            spieler = new List<Spieler>();
            stich = new List<Spielkarte>();
            skat = new List<Spielkarte>();

            if (reizAction == null)
            {
                reizAction += weiterreizen;
            }
            

            if (Einstellungen.humanplayer)
            {
                mensch = new MenschenSpieler(panelMensch, reizAction);
                spieler.Add(mensch);
            }
            else
            {
                bot_0 = new KISpieler(panelMensch, reizAction, Einstellungen.bereitschaft0);
                bot_0.name = Einstellungen.computername;
                spieler.Add(bot_0);
            }

            bot_1 = new KISpieler(panelKI1, reizAction, Einstellungen.bereitschaft1);
            bot_1.name = Einstellungen.gegner1name;
            bot_2 = new KISpieler(panelKI2, reizAction, Einstellungen.bereitschaft2);
            bot_2.name = Einstellungen.gegner2name;

            spieler.Add(bot_1);
            spieler.Add(bot_2);

            createKarten();
            Random r = new Random();
            neuesSpielAction.Invoke(spieler[r.Next(0, 2)]);

        }

        /// <summary>
        /// Methode zum Starten des Spiels
        /// Falls Geber nicht angegeben wird einer zufällig aus den Spieler gewählt
        /// </summary>
        /// <param name="geber"></param>
        internal void neuesSpiel(Spieler geber)
        {
            if (geber == null)
            {
                Random r = new Random();
                geber = spieler[r.Next(0, 2)];
            }

            foreach (Spieler sp in spieler)
            {
                kartenStapel.AddRange(sp.kartenAbgeben());
                sp.purgeCards();
            }
            stich.Clear();
            Debug.Assert(kartenStapel.Count == 32, "Falsche Kartenzahl!");
            runde = 0;
            aktuellerReizwert = 18;
            nullspiel = false;
            ueberreizt = false;
            verloren = false;
            ausgepasst = false;
            alleinSpieler = null;
            running = true;

#if DEBUG
            rtb.Text += "----------------------------\n";
#endif
            panelKI1.Controls.Clear();
            panelKI2.Controls.Clear();
            panelMensch.Controls.Clear();
            panelStiche.Controls.Clear();

            ausgewertet = false;

            if (kartenStapel.Count != 32)
            {
                throw new Exception("Falsche Kartenzahl!");
            }
            // Mischen und austeilen der Karten
            for (int i = 0; i < 7; i++)
            {
                shuffleKarten();
            }

            List<int> geben = new List<int> { 3, 4, 3 };

            foreach (int kartenzahl in geben)
            {
                // Nach der ersten Geberunde wird der Skat gebildet
                if (kartenzahl == 4)
                {
                    skat.Add(kartenStapel[0]);
                    skat.Add(kartenStapel[1]);
                    kartenStapel.RemoveRange(0, 2);
                }

                foreach (Spieler sp in spieler)
                {
                    austeilen(sp, kartenzahl);
                }
            }

#if DEBUG
            Debug.Assert(spieler[0].hand.Count == 10, "Ein Spieler hat die falsche Kartenzahl in der Hand!");
            Debug.Assert(spieler[1].hand.Count == 10, "Ein Spieler hat die falsche Kartenzahl in der Hand!");
            Debug.Assert(spieler[2].hand.Count == 10, "Ein Spieler hat die falsche Kartenzahl in der Hand!");
            Debug.Assert(skat.Count == 2, "Skat hat falsche Kartenzahl!");
#endif

            // Reizen
            foreach (Spieler s in spieler)
            {
                s.calcReizwert();
                s.reizen();                
                s.OnCardChosen -= proceed;
                s.OnCardChosen += proceed;
            }

            // Vorhand bestimmen            
            vorhand = links(geber);
            mittelhand = links(vorhand);
            hinterhand = rechts(vorhand);

            // Reizen
            weiterreizen();     
        }

        /// <summary>
        /// Erstellt den Kartenstapel (32)
        /// </summary>
        private void createKarten()
        {
            kartenStapel.Clear();
            foreach (Kartenfarbe farbe in Kartenfarbe.GetValues(typeof(Kartenfarbe)))
            {
                foreach (Kartenwert wert in Kartenwert.GetValues(typeof(Kartenwert)))
                {
                    Spielkarte neueKarte = new Spielkarte(farbe, wert);
                    if (neueKarte.Wert == Kartenwert.Bube)
                    {
                        neueKarte.isTrumpf = true;
                    }
                    kartenStapel.Add(neueKarte);
                }
            }

            if (kartenStapel.Count != 32)
            {
                throw new ApplicationException();
            }
        }

        /// <summary>
        /// Mischmethode
        /// </summary>
        private void shuffleKarten()
        {
            Random rand = new Random();

            Utility.Shuffle(kartenStapel, rand);

            if (kartenStapel.Count != 32)
            {
                throw new ApplicationException("Falsche Kartenzahl!");
            }
        }

        /// <summary>
        /// Hilfsmethode zum Austeilen der Karten
        /// Kartenstapel wird automatisch angezapft und verringert
        /// </summary>
        /// <param name="sp"></param>
        /// <param name="anzahl"></param>
        private void austeilen(Spieler sp, int anzahl)
        {
            for (int i = 0; i < anzahl; i++)
            {
                sp.giveSpielkarte(kartenStapel[i]);
            }

            kartenStapel.RemoveRange(0, anzahl);
        }

        /// <summary>
        /// Delegiert die Spielansage (Taufe) an die Spieler
        /// </summary>
        public void taufe()
        {
            if (taufeAction == null)
            {
                taufeAction += play;
            }
            
#if DEBUG
            rtb.Text += "Solo: " + alleinSpieler.name + "\n";
#endif
            alleinSpieler.giveSkat(skat);
            alleinSpieler.taufe(taufeAction);
            System.Threading.Thread.Sleep(Einstellungen.speed);
            return;            
        }
        #endregion

        #region Spiellogik
        delegate void chooseCardDelegate(List<Spielkarte> stich, Action<Spieler, Spielkarte> proceedDel);

        /// <summary>
        /// Wichtige Spielmethode, die das Spiel festlegt und weiter einleitet
        /// wird von dem Alleinspieler aufgerufen
        /// </summary>
        /// <param name="art"></param>
        /// <param name="modus"></param>
        /// <param name="spielfarbe"></param>
        public void play(Spielart art, Spielmodus modus, Kartenfarbe spielfarbe)
        {
            spielart = art;
            spielmodus = modus;
            trumpf = spielfarbe;

#if DEBUG2
            List<Spielkarte> tempList = new List<Spielkarte>();
            foreach (Spieler sp in spieler)
            {
                tempList.AddRange(sp.hand);
                tempList.AddRange(sp.erspielteKarten);
                tempList.AddRange(sp.skat);

            }
            tempList.AddRange(kartenStapel);
            tempList.AddRange(stich);
            //tempList.Add(choice);
            if (tempList.Count != 32)
            {
                throw new ArgumentOutOfRangeException();
            }
#endif

            spielwert = Reizen.getReizWert(modus, art, spielfarbe, alleinSpieler.getSpitzenfaktor(spielfarbe) + 1);

            if (art == Spielart.Farbspiel)
            {
                foreach (Spieler sp in spieler)
                {
                    sp.setTrumpf(trumpf);
                }
            }
            else if (art == Spielart.Null)
            {
                foreach (Spieler sp in spieler)
                {
                    sp.setTrumpfNull();
                    nullspiel = true;
                }
            }
            else if (art == Spielart.Grand)
            {
                foreach (Spieler sp in spieler)
                {
                    sp.setTrumpfGrand();
                }
            }

            if (Einstellungen.humanplayer)
            {
                mensch.spielanzeige(art, modus, trumpf);
            }

            if (!alleinSpieler.gedrueckt())
            {
                MessageBox.Show("Keine Karten gedrückt! \n Damit haben Sie leider verloren!", "Schade!");
                verloren = true;
                auswertung();
                // Verloren/Auswertung
            }
            else
            {
                // Spiel beginnen
                aufspieler = vorhand;
                stich.Clear();

                gewinner = aufspieler;

                aufspieler.chooseCard(stich);
                return;
            }
        }

        /// <summary>
        /// Zentrale Spielmethode
        /// Fügt die Karte dem Stich zu, entscheidet automatisch wer Stichgewinner ist
        /// und führt das Spiel weiter.
        /// Wird u.A. von den Spielern aufgerufen
        /// </summary>
        /// <param name="s"></param>
        /// <param name="choice"></param>
        public void proceed(Spieler s, Spielkarte choice)
        {
            if (running)
            {
                #if DEBUG2
                List<Spielkarte> tempList = new List<Spielkarte>();
                foreach (Spieler sp in spieler)
                {
                    tempList.AddRange(sp.hand);
                    tempList.AddRange(sp.erspielteKarten);
                    tempList.AddRange(sp.skat);
                }
                tempList.AddRange(kartenStapel);
                tempList.AddRange(stich);
                tempList.Add(choice);
                if (tempList.Count != 32)
                {
                    throw new ArgumentOutOfRangeException();
                }
                #endif

                stich.Add(choice);
#if DEBUG
                rtb.Text += choice.Farbe.ToString() + " " + choice.Wert.ToString() + "\n";
#endif

                if (!Einstellungen.ohnepause)
                {
                    drawStich(spieler.IndexOf(aufspieler));
                }
                System.Threading.Thread.Sleep(Einstellungen.speed);

                if (besteKarte == null)
                {
                    besteKarte = choice;
                }
                else
                {
                    if (Utility.bessereKarte(choice, besteKarte, s.abwerfen))
                    {
                        besteKarte = choice;
                        gewinner = s;
                    }
                }

                if (stich.Count < 3)
                {
                    links(s).chooseCard(stich);
                    return;
                }
                else
                {
                    ++runde;
                    int sum = 0;
                    foreach (Spielkarte k in stich)
                    {
                        sum += Punktwerte.getPunktwert(k);
                    }
#if DEBUG
                    rtb.Text += "Beste: " + besteKarte.Wert.ToString() + " " + besteKarte.Farbe.ToString() + " " + sum + "\n";
                    rtb.Text += gewinner.name + "\n";
#endif
                    if (gewinner == alleinSpieler && nullspiel)
                    {
                        verloren = true;
                        auswertung();
                        return;
                    }
                    gewinner.giveErspielteKarten(stich);
                    System.Threading.Thread.Sleep(Einstellungen.speed);
                    InvalidateAll();
                    besteKarte = null;
                    aufspieler = gewinner;

                    if (runde < 10)
                    {
                        stich.Clear();
                        aufspieler.chooseCard(stich);
                        return;
                    }
                    else
                    {
                        auswertung();
                        return;
                    }
                }
            }
        }
        #endregion

        

        #region Spielende
        /// <summary>
        /// Methode für die Spielauswertung
        /// Ruft auch Methode aus der MainForm auf, die evtl ein neues Spiel einleitet
        /// </summary>
        public void auswertung()
        {
            falschauswerten++;
            if (!ausgewertet)
            {
                ausgewertet = true;

                if (ausgepasst)
                {
                    if (!Einstellungen.ohnepause)
                    {
                        MessageBox.Show("Das Spiel wurde ausgepasst!");
                    }
                    foreach (Spieler sp in spieler)
                    {
                        kartenStapel.AddRange(sp.kartenAbgeben());
                        stich.Clear();
                    }
                    kartenStapel.AddRange(skat);
                    skat.Clear();
                    saveHighscore();
                    neuesSpielAction.Invoke(vorhand);
                    return;
                }

                solopunkte = alleinSpieler.sumPunkte();

                if (solopunkte > 90)
                {
                    spielwert = Reizen.getReizWert(spielmodus, spielart, trumpf, alleinSpieler.Spitzenfaktor+2);
                }
                else if (solopunkte == 120)
                {
                    spielwert = Reizen.getReizWert(spielmodus, spielart, trumpf, alleinSpieler.Spitzenfaktor + 3);
                }

                if (spielwert < aktuellerReizwert)
                {
                    verloren = true;
                    ueberreizt = true;
                }
                else if (!nullspiel)
                {
                    if (solopunkte < 61)
                    {
                        verloren = true;
                    }
                }
                else if (alleinSpieler.erspielteKarten.Count > 0)
                {
                    verloren = true;
                }

#if DEBUG
                if (verloren) {
                	rtb.Text += verloren.ToString() + "\n";
                }
                else {
                	rtb.Text += (!verloren).ToString() + "\n";
                }
#endif
                if (!Einstellungen.ohnepause)
                {
                    if (verloren)
                    {
                        if (ueberreizt)
                        {
                            MessageBox.Show("Der Alleinspieler hat sich überreizt und damit automatisch verloren! \n"
                                + "Das Spiel war " + spielwert + " Punkte wert.");
                        }
                        else if (nullspiel)
                        {
                            MessageBox.Show("Der Solospieler hat beim Nullspiel Karten bekommen und damit automatisch verloren! \n"
                                + "Das Spiel war " + spielwert + " Punkte wert.");
                        }
                        else
                        {
                            MessageBox.Show("Die Alten haben mit " + (120 - solopunkte).ToString() + " Punkten gewonnen!\n"
                                    + "Alleinspieler hat " + solopunkte.ToString() + " erreicht.\n"
                                    + "Das Spiel war " + spielwert + " Punkte wert.");
                            verloren = true;
                        }
                    }
                    else
                    {
                        if (nullspiel)
                        {
                            MessageBox.Show("Der Solospieler hat gewonnen!");
                        }
                        else
                        {
                            MessageBox.Show("Alleinspieler hat mit " + solopunkte.ToString() + " Punkten gewonnen!\n"
                                + "Die Alten haben + " + (120 - solopunkte).ToString() + " erreicht.\n"
                                + "Das Spiel war " + spielwert + " Punkte wert.");
                        }

                    }
                }               


                
                saveHighscore();
            }
            running = false;
            InvalidateAll();
            neuesSpielAction.Invoke(vorhand);
        }

        /// <summary>
        /// Speichert das aktuelle Spiel in der Rundentabelle und fügt nicht vorhandene Spieler hinzu
        /// </summary>
        private void saveHighscore()
        {
            DBVerbindung db = new DBVerbindung();

            foreach (Spieler sp in spieler)
            {
                db.addToSpieler(Einstellungen.dbPath, sp.name, " ");
            }

            if (ausgepasst)
            {
                db.addToSpiele(Einstellungen.dbPath,
                    spieler[0].name,
                    spieler[1].name,
                    spieler[2].name,
                    spielart.ToString(),
                    spielmodus.ToString(),
                    trumpf.ToString(),
                    aktuellerReizwert,
                    spielwert,
                    !verloren,
                    solopunkte,
                    120 - solopunkte,
                    ausgepasst);
            }
            else
            {
                db.addToSpiele(Einstellungen.dbPath,
                    alleinSpieler.name,
                    links(alleinSpieler).name,
                    rechts(alleinSpieler).name,
                    spielart.ToString(),
                    spielmodus.ToString(),
                    trumpf.ToString(),
                    aktuellerReizwert,
                    spielwert,
                    !verloren,
                    solopunkte,
                    120 - solopunkte,
                    ausgepasst);
            }
        }

        /// <summary>
        /// Aktualisiert die Anzahl der Spiele in der Spielertabelle
        /// </summary>
        public void updateSpielerSpiele()
        {
            DBVerbindung db = new DBVerbindung(); 

            foreach (Spieler sp in spieler)
            {
                db.updateSpielAnzahl(Einstellungen.dbPath, sp.name);
            }
        }
        
        #endregion

        #region Drawing
        /// <summary>
        /// Die Arbeit des Backgroundworkers
        /// Hier wird das zeichnen des Stiches verwaltet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundworkerStich_DoWork(object sender, DoWorkEventArgs e)
        {
        	if (!Einstellungen.ohnepause) {
            drawStich(spieler.IndexOf(aufspieler));
        	}
        }

        /// <summary>
        /// Zeichnen-Methode für den Stich
        /// </summary>
        /// <param name="aufspieler"></param>
        private void drawStich(int aufspieler)
        {
            //panelStiche.Refresh();
            //panelStiche.Invalidate();

            gc.ResetTransform();
            gc.TranslateTransform(panelStiche.Width / 2, panelStiche.Height / 2);
            //gc.ScaleTransform(0.8F,0.8F);
            gc.RotateTransform(120 * aufspieler);
            gc.TranslateTransform(0, 60);

            foreach (Spielkarte k in stich)
            {
                k.BackColor = k.backcolor;
                k.drawUpside(gc);
                //panelStiche.Controls.Add(k);
                gc.RotateTransform(120);
                gc.TranslateTransform(0, 60);
                //gc.DrawString(k.Farbe.ToString() + " " + k.Wert.ToString(), 
                //    panelStiche.Font, Brushes.Black, 
                //    20,0 );


            }
        }

        /// <summary>
        /// Paint-Methode für den Stich
        /// Delegiert an einen Backgroundworker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void panelStiche_Paint(object sender, PaintEventArgs e)
        {
            if (!backgroundworkerStich.IsBusy)
            {
                backgroundworkerStich.RunWorkerAsync();
            }
        }
        #endregion

        #region Andere
        /// <summary>
        /// Liefert den linken Spieler anhand der Spielerliste
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private Spieler links(Spieler s)
        {
            return this.spieler[(spieler.IndexOf(s) + 1) % 3];
        }

        /// <summary>
        /// Liefert den rechten Spieler anhand der Spielerliste
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private Spieler rechts(Spieler s)
        {
            return this.spieler[(spieler.IndexOf(s) + 2) % 3];
        }
        #endregion

        /// <summary>
        /// Erzwingt neuzeichnen aller Panels
        /// </summary>
        internal void InvalidateAll()
        {
            panelKI1.Invalidate();
            panelKI2.Invalidate();
            panelMensch.Invalidate();
            panelStiche.Invalidate();
        }

        /// <summary>
        /// Aufräummethode
        /// </summary>
        internal void clear()
        {
            running = false;
            kartenStapel = null;
            foreach (Spieler sp in spieler)
            {
                sp.purgeCards();
                stich.Clear();
            }
        }
    }
}

