﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace SkatSharp.Documentation
{
    static class XMLTransform
    {
        public static void Transform(string sXmlPath, string sXslPath)
        {

            try
            {

                //load the Xml doc
                XPathDocument myXPathDoc = new XPathDocument(sXmlPath);


                XslCompiledTransform myXslTrans = new XslCompiledTransform();

                //load the Xsl 
                myXslTrans.Load(sXslPath);

                //create the output stream
                XmlTextWriter myWriter = new XmlTextWriter
                    ("result.html", null);

                //do the actual transform of Xml
                myXslTrans.Transform(myXPathDoc, null, myWriter);

                myWriter.Close();


            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                {
                    MessageBox.Show(e.Message + "\n" + e.InnerException.Message);
                }
                //MessageBox.Show(e.Message);
            }

        }
    }
}
