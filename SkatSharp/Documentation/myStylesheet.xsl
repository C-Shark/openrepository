﻿<xsl:stylesheet 
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      version="1.0">

<xsl:template match="/">
  <html>
  <body>
  <h2>
<xsl:value-of select="doc/assembly/name"/>
</h2>
<table  width="400" border="0">

    <tr bgcolor="#DDDDDD">
      <th style="word-break:break-all;word-wrap:break-word">Member</th>
     <th>Parameters</th>
     <th>Returns</th>
    </tr> 
    <xsl:for-each select="doc/members/member">
<xsl:sort select="@name"/>
    <tr bgcolor="#EEEEEE">     
      <td><b>Name: </b><xsl:value-of select="@name"/></td><td></td><td></td></tr>
<tr bgcolor="#EEEEFF">
     <td><b>Summary:</b> <br /><xsl:value-of select="summary"/></td>
<td>
        <xsl:for-each select="param">
          <xsl:value-of select ="@name"/> <br />
        </xsl:for-each>
</td>
<td>
          <xsl:value-of select ="returns"/>
</td>
    </tr>
<tr><td></td></tr>
    </xsl:for-each>
  </table>
  
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>