﻿using System;
using System.Windows.Forms;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace SkatSharp
{
    static class Program
    {
        /// <summary>
        /// Startmethode
        /// </summary>
        static void run()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        /// <summary>
        /// Laden der Einstellung.ini
        /// </summary>
        static void loadsettings()
        {
            try
            {
                FileStream fs = new FileStream(Application.StartupPath + "\\Einstellungen.ini", FileMode.Open);
                StreamReader reader = new StreamReader(fs);
                String line;

                while (!reader.EndOfStream)
                {
                    line = reader.ReadLine();
                    if (!line.Contains("["))
                    {
                        try
                        {
                            //if (line.Contains("aktuellerSpieler"))
                            //{
                            //    Einstellungen.aktuellerSpieler = line.Split(':')[1];
                            //}
                            if (line.Contains("gegner1name"))
                            {
                                Einstellungen.gegner1name = line.Split(':')[1];
                            }
                            else if (line.Contains("gegner2name"))
                            {
                                Einstellungen.gegner2name = line.Split(':')[1];
                            }
                            else if (line.Contains("computername"))
                            {
                                Einstellungen.computername = line.Split(':')[1];
                            }
                            else if (line.Contains("humanplayer"))
                            {
                                Einstellungen.humanplayer = Convert.ToBoolean(line.Split(':')[1]);
                            }
                            else if (line.Contains("maxRunden"))
                            {
                                Einstellungen.maxRunden = Convert.ToInt32(line.Split(':')[1]);
                            }
                            else if (line.Contains("maxSpiele"))
                            {
                                Einstellungen.maxSpiele = Convert.ToInt32(line.Split(':')[1]);
                            }
                            else if (line.Contains("speed"))
                            {
                                Einstellungen.speed = Convert.ToInt32(line.Split(':')[1]);
                            }
                            else if (line.Contains("kispeed"))
                            {
                                Einstellungen.kispeed = Convert.ToInt32(line.Split(':')[1]);
                            }
                            else if (line.Contains("ohnepause"))
                            {
                                Einstellungen.ohnepause = Convert.ToBoolean(line.Split(':')[1]);
                            }
                            else if (line.Contains("bereitschaft1"))
                            {
                                Einstellungen.bereitschaft1 = Convert.ToInt32(line.Split(':')[1]);
                            }
                            else if (line.Contains("bereitschaft2"))
                            {
                                Einstellungen.bereitschaft2 = Convert.ToInt32(line.Split(':')[1]);
                            }
                            else if (line.Contains("bereitschaft0"))
                            {
                                Einstellungen.bereitschaft0 = Convert.ToInt32(line.Split(':')[1]);
                            }
                        }
                        catch (ArithmeticException ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }

            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Einstellungen nicht gefunden!", "Fehler!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                Einstellungen.gegner1name = "";
                Einstellungen.gegner2name = "";
                Einstellungen.computername = "";
                Einstellungen.humanplayer = false;
                Einstellungen.maxRunden = 1;
                Einstellungen.maxSpiele = 3;
                Einstellungen.speed = 0;//1000;
                Einstellungen.kispeed = 0;//1000;
                Einstellungen.ohnepause = true;//false;
                Einstellungen.bereitschaft0 = 40;
                Einstellungen.bereitschaft1 = 60;
                Einstellungen.bereitschaft2 = 50;
            }
            catch (FileLoadException ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Laden
            loadsettings();

            // Erzeugen der Dokumentation als XML->XSL
            Documentation.XMLTransform.Transform("SkatSharp.XML", "myStylesheet.xsl");

            // Start
            run();
            
        }
    }
}
