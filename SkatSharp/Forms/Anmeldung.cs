﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace SkatSharp
{
    /// <summary>
    /// Fenster zum Anmelden eines Spielers
    /// Auch bei reinen KI-Spiel erforderlich
    /// </summary>
    public partial class Anmeldung : Form
    {
         DB.DBVerbindung db;

        private DataTable spieler;

        public Anmeldung()
        {
            InitializeComponent();

            db = new DB.DBVerbindung();

            spieler = db.getSpielerListe(Einstellungen.dbPath);

            comboBoxSpieler.DataSource = spieler;
            comboBoxSpieler.DisplayMember = "Pseudonym";
            comboBoxSpieler.ValueMember = "Pseudonym";
            this.DialogResult = DialogResult.Cancel;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            var rowView = comboBoxSpieler.SelectedItem as DataRowView;
            String pseud = rowView["Pseudonym"].ToString();

            String spielerpw = spieler.Rows[comboBoxSpieler.SelectedIndex]["Passwort"].ToString();

            try
            {
                if (spielerpw == textBoxPW.Text)
                {
                    Einstellungen.aktuellerSpieler = pseud;
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                    // Spieler wählen
                }
                else
                {
                    throw new ArgumentException("Falsches Passwort!");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            Forms.NeuerSpieler neusp = new Forms.NeuerSpieler();
            neusp.ShowDialog();

            if (neusp.DialogResult == DialogResult.OK)
            {
                spieler = db.getSpielerListe(Einstellungen.dbPath);

                comboBoxSpieler.DataSource = null;
                comboBoxSpieler.DataSource = spieler;
                comboBoxSpieler.DisplayMember = "Pseudonym";
                comboBoxSpieler.ValueMember = "Pseudonym";
            }
            
        }

        private void Anmeldung_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Application.Exit();
        }

        private void textBoxPW_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                buttonOK_Click(null, EventArgs.Empty);
            }
        }
    }
}
