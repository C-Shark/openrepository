﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SkatSharp
{
    /// <summary>
    /// Fenster für globale Einstellungen
    /// </summary>
    public partial class EinstellungenForm : Form
    {
        public EinstellungenForm()
        {
            InitializeComponent();

            trackBarSpeed.Value = Einstellungen.speed;
            trackBarKISpeed.Value = Einstellungen.kispeed;
            checkBoxPause.Checked = Einstellungen.ohnepause;
        }


        private void buttonTake_Click(object sender, EventArgs e)
        {
            Einstellungen.speed = trackBarSpeed.Value;
            Einstellungen.kispeed = trackBarKISpeed.Value;
            Einstellungen.ohnepause = checkBoxPause.Checked;
            Einstellungen.mogeln = checkBoxCheat.Checked;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            trackBarSpeed.Value = Einstellungen.speed;
            trackBarKISpeed.Value = Einstellungen.kispeed;
            checkBoxPause.Checked = Einstellungen.ohnepause;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            buttonTake_Click(sender, e);
            this.Close();
        }
    }
}
