﻿namespace SkatSharp
{
    partial class Statistik
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxSpieleSolo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxSpieleGesamt = new System.Windows.Forms.TextBox();
            this.textBoxGewonnen = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelKreuz = new System.Windows.Forms.Label();
            this.labelHerz = new System.Windows.Forms.Label();
            this.labelPik = new System.Windows.Forms.Label();
            this.labelKaro = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.labelFarbspiele = new System.Windows.Forms.Label();
            this.labelGrand = new System.Windows.Forms.Label();
            this.labelNull = new System.Windows.Forms.Label();
            this.labelHand = new System.Windows.Forms.Label();
            this.labelOuvert = new System.Windows.Forms.Label();
            this.labelOuvertHand = new System.Windows.Forms.Label();
            this.labelReizwert = new System.Windows.Forms.Label();
            this.labelSpielwert = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.labelPunkte = new System.Windows.Forms.Label();
            this.buttonClose = new System.Windows.Forms.Button();
            this.comboBoxSpieler = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Spieler";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Gespielte Spiele:";
            // 
            // textBoxSpieleSolo
            // 
            this.textBoxSpieleSolo.Enabled = false;
            this.textBoxSpieleSolo.Location = new System.Drawing.Point(128, 120);
            this.textBoxSpieleSolo.Name = "textBoxSpieleSolo";
            this.textBoxSpieleSolo.Size = new System.Drawing.Size(100, 20);
            this.textBoxSpieleSolo.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Gewonnen:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Spiele gesamt:";
            // 
            // textBoxSpieleGesamt
            // 
            this.textBoxSpieleGesamt.Enabled = false;
            this.textBoxSpieleGesamt.Location = new System.Drawing.Point(128, 88);
            this.textBoxSpieleGesamt.Name = "textBoxSpieleGesamt";
            this.textBoxSpieleGesamt.Size = new System.Drawing.Size(100, 20);
            this.textBoxSpieleGesamt.TabIndex = 5;
            // 
            // textBoxGewonnen
            // 
            this.textBoxGewonnen.Enabled = false;
            this.textBoxGewonnen.Location = new System.Drawing.Point(128, 160);
            this.textBoxGewonnen.Name = "textBoxGewonnen";
            this.textBoxGewonnen.Size = new System.Drawing.Size(100, 20);
            this.textBoxGewonnen.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 208);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Kreuz:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(136, 208);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Pik:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 240);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Herz:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(136, 240);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Karo:";
            // 
            // labelKreuz
            // 
            this.labelKreuz.AutoSize = true;
            this.labelKreuz.Location = new System.Drawing.Point(72, 208);
            this.labelKreuz.Name = "labelKreuz";
            this.labelKreuz.Size = new System.Drawing.Size(0, 13);
            this.labelKreuz.TabIndex = 11;
            // 
            // labelHerz
            // 
            this.labelHerz.AutoSize = true;
            this.labelHerz.Location = new System.Drawing.Point(72, 240);
            this.labelHerz.Name = "labelHerz";
            this.labelHerz.Size = new System.Drawing.Size(0, 13);
            this.labelHerz.TabIndex = 12;
            // 
            // labelPik
            // 
            this.labelPik.AutoSize = true;
            this.labelPik.Location = new System.Drawing.Point(176, 208);
            this.labelPik.Name = "labelPik";
            this.labelPik.Size = new System.Drawing.Size(0, 13);
            this.labelPik.TabIndex = 13;
            // 
            // labelKaro
            // 
            this.labelKaro.AutoSize = true;
            this.labelKaro.Location = new System.Drawing.Point(176, 240);
            this.labelKaro.Name = "labelKaro";
            this.labelKaro.Size = new System.Drawing.Size(0, 13);
            this.labelKaro.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(248, 208);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Farbspiele:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(248, 232);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Grand:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(248, 256);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "Null:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(352, 208);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "Hand:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(352, 232);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "Ouvert:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(352, 256);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 13);
            this.label14.TabIndex = 20;
            this.label14.Text = "Ouvert/Hand:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 296);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(139, 13);
            this.label15.TabIndex = 21;
            this.label15.Text = "Durchschnittlicher Reizwert:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(16, 320);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(141, 13);
            this.label16.TabIndex = 22;
            this.label16.Text = "Durchschnittlicher Spielwert:";
            // 
            // labelFarbspiele
            // 
            this.labelFarbspiele.AutoSize = true;
            this.labelFarbspiele.Location = new System.Drawing.Point(312, 208);
            this.labelFarbspiele.Name = "labelFarbspiele";
            this.labelFarbspiele.Size = new System.Drawing.Size(0, 13);
            this.labelFarbspiele.TabIndex = 23;
            // 
            // labelGrand
            // 
            this.labelGrand.AutoSize = true;
            this.labelGrand.Location = new System.Drawing.Point(312, 232);
            this.labelGrand.Name = "labelGrand";
            this.labelGrand.Size = new System.Drawing.Size(0, 13);
            this.labelGrand.TabIndex = 24;
            // 
            // labelNull
            // 
            this.labelNull.AutoSize = true;
            this.labelNull.Location = new System.Drawing.Point(312, 256);
            this.labelNull.Name = "labelNull";
            this.labelNull.Size = new System.Drawing.Size(0, 13);
            this.labelNull.TabIndex = 25;
            // 
            // labelHand
            // 
            this.labelHand.AutoSize = true;
            this.labelHand.Location = new System.Drawing.Point(432, 208);
            this.labelHand.Name = "labelHand";
            this.labelHand.Size = new System.Drawing.Size(0, 13);
            this.labelHand.TabIndex = 26;
            // 
            // labelOuvert
            // 
            this.labelOuvert.AutoSize = true;
            this.labelOuvert.Location = new System.Drawing.Point(432, 232);
            this.labelOuvert.Name = "labelOuvert";
            this.labelOuvert.Size = new System.Drawing.Size(0, 13);
            this.labelOuvert.TabIndex = 27;
            // 
            // labelOuvertHand
            // 
            this.labelOuvertHand.AutoSize = true;
            this.labelOuvertHand.Location = new System.Drawing.Point(432, 256);
            this.labelOuvertHand.Name = "labelOuvertHand";
            this.labelOuvertHand.Size = new System.Drawing.Size(0, 13);
            this.labelOuvertHand.TabIndex = 28;
            // 
            // labelReizwert
            // 
            this.labelReizwert.AutoSize = true;
            this.labelReizwert.Location = new System.Drawing.Point(168, 296);
            this.labelReizwert.Name = "labelReizwert";
            this.labelReizwert.Size = new System.Drawing.Size(0, 13);
            this.labelReizwert.TabIndex = 29;
            // 
            // labelSpielwert
            // 
            this.labelSpielwert.AutoSize = true;
            this.labelSpielwert.Location = new System.Drawing.Point(168, 320);
            this.labelSpielwert.Name = "labelSpielwert";
            this.labelSpielwert.Size = new System.Drawing.Size(0, 13);
            this.labelSpielwert.TabIndex = 30;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(248, 296);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(129, 13);
            this.label17.TabIndex = 31;
            this.label17.Text = "Durchschnittliche Punkte:";
            // 
            // labelPunkte
            // 
            this.labelPunkte.AutoSize = true;
            this.labelPunkte.Location = new System.Drawing.Point(392, 296);
            this.labelPunkte.Name = "labelPunkte";
            this.labelPunkte.Size = new System.Drawing.Size(0, 13);
            this.labelPunkte.TabIndex = 32;
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(416, 336);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 34;
            this.buttonClose.Text = "Schließen";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // comboBoxSpieler
            // 
            this.comboBoxSpieler.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSpieler.FormattingEnabled = true;
            this.comboBoxSpieler.Location = new System.Drawing.Point(24, 48);
            this.comboBoxSpieler.Name = "comboBoxSpieler";
            this.comboBoxSpieler.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSpieler.TabIndex = 35;
            this.comboBoxSpieler.SelectedValueChanged += new System.EventHandler(this.comboBoxSpieler_SelectedValueChanged);
            // 
            // Statistik
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 375);
            this.ControlBox = false;
            this.Controls.Add(this.comboBoxSpieler);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.labelPunkte);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.labelSpielwert);
            this.Controls.Add(this.labelReizwert);
            this.Controls.Add(this.labelOuvertHand);
            this.Controls.Add(this.labelOuvert);
            this.Controls.Add(this.labelHand);
            this.Controls.Add(this.labelNull);
            this.Controls.Add(this.labelGrand);
            this.Controls.Add(this.labelFarbspiele);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.labelKaro);
            this.Controls.Add(this.labelPik);
            this.Controls.Add(this.labelHerz);
            this.Controls.Add(this.labelKreuz);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxGewonnen);
            this.Controls.Add(this.textBoxSpieleGesamt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxSpieleSolo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Statistik";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Statistik";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxSpieleSolo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxSpieleGesamt;
        private System.Windows.Forms.TextBox textBoxGewonnen;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelKreuz;
        private System.Windows.Forms.Label labelHerz;
        private System.Windows.Forms.Label labelPik;
        private System.Windows.Forms.Label labelKaro;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelFarbspiele;
        private System.Windows.Forms.Label labelGrand;
        private System.Windows.Forms.Label labelNull;
        private System.Windows.Forms.Label labelHand;
        private System.Windows.Forms.Label labelOuvert;
        private System.Windows.Forms.Label labelOuvertHand;
        private System.Windows.Forms.Label labelReizwert;
        private System.Windows.Forms.Label labelSpielwert;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label labelPunkte;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.ComboBox comboBoxSpieler;
    }
}