﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SkatSharp
{
    /// <summary>
    /// Hauptfenster für das Spiel
    /// </summary>
    public partial class MainForm : Form
    {

        private Spiel dasSpiel;

        private int spielzahl = 1;
        private Spieler aktuellerGeber;

        public MainForm()
        {
            InitializeComponent();

            if (String.IsNullOrEmpty(Einstellungen.aktuellerSpieler))
            {
                enableSpieler();
            }
            else
            {
                disableSpieler();
            }

            this.spielBeendenToolStripMenuItem.Enabled = false;

#if DEBUG
            this.Size = new Size(1054, 638);
            
#else
            this.Size = new Size(800, 638);
            
#endif
            this.DoubleBuffered = true;
            
        }
        
        void NeuToolStripMenuItemClick(object sender, EventArgs e)
        {
            spielzahl = 1;

            if (dasSpiel != null)
            {
                if (MessageBox.Show("Neues Spiel starten?", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.Cancel)
                {
                    return;
                }
            }
            if (String.IsNullOrEmpty(Einstellungen.aktuellerSpieler))
            {
                Anmeldung anmeldung = new Anmeldung();

                anmeldung.ShowDialog();

                enableSpieler();
                if (anmeldung.DialogResult == DialogResult.Cancel)
                {
                    return;
                }
            }

            DB.DBVerbindung db = new DB.DBVerbindung();
            db.createRundentabelle(Einstellungen.dbPath);

            StartDialog sd = new StartDialog();
            sd.ShowDialog();

            if (sd.DialogResult == DialogResult.Cancel)
            {
                return;
            }
            dasSpiel = new Spiel(panelStiche, 
                panelSpieler, 
                panelGegner1, 
                panelGegner2, 
                this.RTB, 
                new Action<Spieler>(neuesSpiel));

            this.spielBeendenToolStripMenuItem.Enabled = true;

        	dasSpiel.setUp();
        }

        private void enableSpieler()
        {
            datenÄndernToolStripMenuItem.Enabled = true;
            kontoSchließenToolStripMenuItem.Enabled = true;
            toolStripStatusLabelSpieler.Text = Einstellungen.aktuellerSpieler;
        }

        private void disableSpieler()
        {
            datenÄndernToolStripMenuItem.Enabled = false;
            kontoSchließenToolStripMenuItem.Enabled = false;
            toolStripStatusLabelSpieler.Text = "";
        }

        private void neuesSpiel(Spieler geber)
        {
            if (aktuellerGeber == null)
            {
                this.aktuellerGeber = geber;
            }               
            
            if (Einstellungen.ohnepause && !Einstellungen.humanplayer)
            {
                buttonSpielen_Click(null, EventArgs.Empty);
            }
            else
            {
                if (spielzahl == 0)
                {
                    buttonSpielen.Text = "Los!";
                }
                else
                {
                    buttonSpielen.Text = "Weiter!";
                }
                buttonSpielen.Enabled = true;
                buttonSpielen.Visible = true;
                buttonDrawNew.Visible = true;
            }
        }

        private void MainForm_ResizeEnd(object sender, EventArgs e)
        {
            //float dx = this.Width / (float)x;
            //float dy = this.Height / (float)y;

            //this.panelGegner1.Width = (int)((float)this.panelGegner1.Width * dx);
            //this.panelGegner1.Height = (int)((float)this.panelGegner1.Width * dy);

            //this.panelSpieler.Width = (int)((float)this.panelSpieler.Width * dx);
            //this.panelSpieler.Height = (int)((float)this.panelSpieler.Width * dy);

            //this.panelStiche.Width = (int)((float)this.panelStiche.Width * dx);
            //this.panelStiche.Height = (int)((float)this.panelStiche.Width * dy);
            
        }

        private void MainForm_ResizeBegin(object sender, EventArgs e)
        {
            //x = this.Width;
            //y = this.Height;

        }

        private void statistikAnzeigenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Statistik statistik = new Statistik();
            statistik.ShowDialog();            
        }

        private void regelnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("Regeln\\ISkO2011.pdf");
            }
            catch (Exception ex)
            {
                if (ex.Message == "Das System kann die angegebene Datei nicht finden")
                {
                    if (MessageBox.Show(
            "Möchten sie die Regeln im Browser öffnen? \n (Internetverbindung erforderlich)",
            "Datei nicht gefunden!",
            MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk
            ) == DialogResult.Yes)
                    {
                        System.Diagnostics.Process.Start("https://www.google.de/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0CCIQFjAA&url=http%3A%2F%2Fwww.dskv.de%2Fupload_user%2Fskatgericht%2FPDF%2FISkO.pdf&ei=1E6hU9yJM-nf4QTt7YEI&usg=AFQjCNFd__7kfGy3hJ2XgyaRhJ7AfYi7Mg&bvm=bv.69137298,d.bGQ");
                    }
                }
                else
                {
                    throw;
                }
            }
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }

        private void einstellungenMenuItem_Click(object sender, EventArgs e)
        {
            EinstellungenForm eForm = new EinstellungenForm();
            eForm.ShowDialog();
        }

        private void programmBeendenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonSpielen_Click(object sender, EventArgs e)
        {
            buttonSpielen.Enabled = false;
            buttonSpielen.Visible = false;
            if (spielzahl <= Einstellungen.maxSpiele)
            {
                spielzahl++;
                //dasSpiel = new Spiel(panelStiche, panelSpieler, panelGegner1, panelGegner2, this.RTB, new Action<Spieler>(neuesSpiel));

                //dasSpiel.setUp();
                dasSpiel.neuesSpiel(aktuellerGeber);
            }
            else
            {
                Forms.HighScore hs = new Forms.HighScore(dasSpiel);
                hs.ShowDialog();

                panelGegner1.Controls.Clear();
                panelGegner2.Controls.Clear();
                panelSpieler.Controls.Clear();
                panelStiche.Controls.Clear();
            }
        }

        private void spielBeendenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dasSpiel != null)
            {
                Forms.HighScore hs = new Forms.HighScore(dasSpiel);
                hs.ShowDialog();

                aktuellerGeber = null;
                dasSpiel.clear();
                dasSpiel = null;
                buttonSpielen.Enabled = false;
                buttonSpielen.Visible = false;
                buttonDrawNew.Visible = false;
                panelGegner1.Controls.Clear();
                panelGegner2.Controls.Clear();
                panelSpieler.Controls.Clear();
                panelStiche.Controls.Clear();
            }
        }

        private void buttonDrawNew_Click(object sender, EventArgs e)
        {
            if (dasSpiel != null)
            {
                dasSpiel.InvalidateAll();

            }
        }

        private void kontoSchließenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Einstellungen.aktuellerSpieler = null;
            disableSpieler();
        }

        private void kontoWechselnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Anmeldung anmeldung = new Anmeldung();

            anmeldung.ShowDialog();
            enableSpieler();
        }

        private void datenÄndernToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Forms.NeuerSpieler nsp = new Forms.NeuerSpieler(Einstellungen.aktuellerSpieler);
            nsp.ShowDialog();
        }

        private void kontoLöschenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Wollen Sie ihre Daten wirklich löschen? \n"
                + "Es werden auch alle Spiele mit Ihnen gelöscht!", "Achtung!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
                == DialogResult.Yes)
            {
                DB.DBVerbindung db = new DB.DBVerbindung();
                try
                {
                    db.deleteSpieler(Einstellungen.dbPath, Einstellungen.aktuellerSpieler);

                    disableSpieler();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
               
            }
        }

        private void hilfeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hier ist noch nichts hinterlegt!", "Handbuch?");
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("by C-Shark \n 2014", "Skat\u266f", MessageBoxButtons.OK, MessageBoxIcon.None);
        }

    }
}
