﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SkatSharp
{
    /// <summary>
    /// Form für Einstellungen zum Spiel
    /// </summary>
    public partial class StartDialog : Form
    {
        public StartDialog()
        {
            InitializeComponent();
            textBoxGegner1.Text = Einstellungen.gegner1name;
            textBoxGegner2.Text = Einstellungen.gegner2name;
            textBoxComputer0.Text = Einstellungen.computername;
            numericUpDownRunden.Value = (int)numericUpDownSpiele.Value / 3;
            numericUpDownSpiele.Value = Einstellungen.maxSpiele;
            checkBoxHuman.Checked = Einstellungen.humanplayer;

            this.numericUpDownSpiele.ValueChanged += new System.EventHandler(this.numericUpDownSpiele_ValueChanged);
        }


        private void numericUpDownSpiele_ValueChanged(object sender, EventArgs e)
        {
            
            if (numericUpDownSpiele.Value < 0)
            {
                numericUpDownSpiele.Value = 0;
            }
            else
            {
                numericUpDownRunden.Value = (int)numericUpDownSpiele.Value / 3 ;
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            Einstellungen.gegner1name = textBoxGegner1.Text;
            Einstellungen.gegner2name = textBoxGegner2.Text;
            Einstellungen.humanplayer = checkBoxHuman.Checked;
            Einstellungen.maxRunden = (int)numericUpDownRunden.Value;
            Einstellungen.maxSpiele = (int)numericUpDownSpiele.Value;

            if (!checkBoxHuman.Checked)
            {
                Einstellungen.computername = textBoxComputer0.Text;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void checkBoxHuman_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxHuman.Checked)
            {
                labelcomputername.Visible = false;
                textBoxComputer0.Visible = false;
            }
            else
            {
                labelcomputername.Visible = true;
                textBoxComputer0.Visible = true;
            }
        }
    }
}
