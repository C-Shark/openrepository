﻿namespace SkatSharp.Forms
{
    partial class NeuerSpieler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.persistentTextBoxPW2 = new SkatSharp.PersistentTextBox();
            this.persistentTextBoxPW1 = new SkatSharp.PersistentTextBox();
            this.persistentTextBoxName = new SkatSharp.PersistentTextBox();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(8, 120);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(96, 120);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 8;
            this.buttonCancel.Text = "Abbrechen";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // persistentTextBoxPW2
            // 
            this.persistentTextBoxPW2.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.persistentTextBoxPW2.Location = new System.Drawing.Point(8, 72);
            this.persistentTextBoxPW2.Name = "persistentTextBoxPW2";
            this.persistentTextBoxPW2.PersistentText = "Passwort wiederholen";
            this.persistentTextBoxPW2.Size = new System.Drawing.Size(160, 20);
            this.persistentTextBoxPW2.TabIndex = 7;
            this.persistentTextBoxPW2.Text = "Passwort wiederholen";
            // 
            // persistentTextBoxPW1
            // 
            this.persistentTextBoxPW1.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.persistentTextBoxPW1.Location = new System.Drawing.Point(8, 40);
            this.persistentTextBoxPW1.Name = "persistentTextBoxPW1";
            this.persistentTextBoxPW1.PersistentText = "Passwort";
            this.persistentTextBoxPW1.Size = new System.Drawing.Size(160, 20);
            this.persistentTextBoxPW1.TabIndex = 6;
            this.persistentTextBoxPW1.Text = "Passwort";
            // 
            // persistentTextBoxName
            // 
            this.persistentTextBoxName.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.persistentTextBoxName.Location = new System.Drawing.Point(8, 8);
            this.persistentTextBoxName.Name = "persistentTextBoxName";
            this.persistentTextBoxName.PersistentText = "Pseudonym";
            this.persistentTextBoxName.Size = new System.Drawing.Size(160, 20);
            this.persistentTextBoxName.TabIndex = 5;
            this.persistentTextBoxName.Text = "Pseudonym";
            // 
            // NeuerSpieler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(187, 161);
            this.ControlBox = false;
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.persistentTextBoxPW2);
            this.Controls.Add(this.persistentTextBoxPW1);
            this.Controls.Add(this.persistentTextBoxName);
            this.Controls.Add(this.buttonOK);
            this.Name = "NeuerSpieler";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Neuen Spieler erstellen";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NeuerSpieler_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private PersistentTextBox persistentTextBoxName;
        private PersistentTextBox persistentTextBoxPW1;
        private PersistentTextBox persistentTextBoxPW2;
        private System.Windows.Forms.Button buttonCancel;
    }
}