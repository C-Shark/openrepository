﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkatSharp
{
    /// <summary>
    /// Spezielle Textbox, die einen festen Text anzeigt, falls nicht anderes in der Box steht
    /// </summary>
    class PersistentTextBox : System.Windows.Forms.TextBox
    {
        private String persistentText = "";
        private event EventHandler PersistentTextChanged;

        public String PersistentText
        {
            get { return persistentText; }
            set { persistentText = value; PersistentTextChanged(this, EventArgs.Empty); }
        }

        public PersistentTextBox()
        {
            this.Text = persistentText;
            this.ForeColor = System.Drawing.SystemColors.InactiveCaption;

            this.PersistentTextChanged += new System.EventHandler(delegate { Text = persistentText; });
            this.Enter += new System.EventHandler(this.OnEnter);
            this.Leave += new System.EventHandler(this.OnLeave);
        }

        private void OnEnter(object sender, EventArgs e)
        {
            if (this.Text == persistentText)
            {
                this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
                this.Text = "";
            }
        }

        private void OnLeave(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(this.Text))
            {
                this.Text = persistentText;
                this.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            }
        }
    }
}
