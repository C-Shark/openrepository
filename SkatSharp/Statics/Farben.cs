﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkatSharp
{
    /// <summary>
    /// Die vier Farben (Mit Spielwerten)
    /// </summary>
    public enum Kartenfarbe
    {
        Kreuz = 12,
        Pik = 11,
        Herz = 10,
        Karo = 9,
    }
}
