﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkatSharp
{
    /// <summary>
    /// Statische Klasse für den Augenwert der Karte
    /// </summary>
    public static class Punktwerte
    {
        /// <summary>
        /// Liefert den Augenwert (Punktewert) der Karte
        /// </summary>
        /// <param name="karte"></param>
        /// <returns></returns>
        public static int getPunktwert(Spielkarte karte)
        {
            if (karte != null)
            {
                switch (karte.Wert)
                {
                    case Kartenwert.Sieben: return 0;
                    case Kartenwert.Acht: return 0;
                    case Kartenwert.Neun: return 0;
                    case Kartenwert.Zehn: return 10;
                    case Kartenwert.Bube: return 2;
                    case Kartenwert.Dame: return 3;
                    case Kartenwert.Koenig: return 4;
                    case Kartenwert.Ass: return 11;
                    default: return -1;
                }
            }
            return -1;
        }
    }
}
