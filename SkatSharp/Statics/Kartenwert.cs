﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkatSharp
{
    /// <summary>
    /// Die Mögliche Werte der Karten 7-ASS
    /// </summary>
    public enum Kartenwert
    {
        Sieben,
        Acht,
        Neun,       
        Bube,
        Dame,
        Koenig,
        Zehn,
        Ass,
    }
}
