﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SkatSharp.Statics
{
    static class Drawing
    {
        static int cardWidthDefault = 50;
        static int cardWidth = cardWidthDefault;
        static int cardHeightDefault = 90;
        static int cardHeight = cardHeightDefault;
        static Color backColor = Color.White;

        public static void drawSpielkarte(Graphics gc, Kartenfarbe farbe, Kartenwert wert, bool upside)
        {
            // Malen des Hintergrundes
            gc.FillRectangle(new SolidBrush(backColor), 0, 0, cardWidth, cardHeight);
            gc.DrawRectangle(Pens.Wheat, 0, 0, cardWidth, cardHeight);

            Color forecolor = Color.Black;

            if (upside)
            {

                switch (farbe)
                {
                    case Kartenfarbe.Kreuz: forecolor = Color.Black;
                        break;
                    case Kartenfarbe.Pik: forecolor = Color.Green;
                        break;
                    case Kartenfarbe.Herz: forecolor = Color.Red;
                        break;
                    case Kartenfarbe.Karo: forecolor = Color.Orange;
                        break;
                    default:
                        break;
                }

                //Point [] rand = new Point[]{
                //    new Point(0,10),
                //    new Point(10,0),                
                //    new Point(this.Width-10,0),
                //    new Point(this.Width,10),
                //    new Point(this.Width,this.Height-10),
                //    new Point(this.Width-10,this.Height),
                //    new Point(10, this.Height),
                //    new Point(0, this.Height-10)            
                //    };

                //gc.DrawClosedCurve(Pens.Yellow,rand, 10F, System.Drawing.Drawing2D.FillMode.Winding);
                //gc.FillClosedCurve(Brushes.Yellow, rand, System.Drawing.Drawing2D.FillMode.Alternate, 10);

                // Malen der Symbole und Farbe

                SolidBrush schriftfarbe = new SolidBrush(forecolor);
                Font schriftart = new Font("Arial", cardWidth / 4);

                Point center = new Point(cardWidth / 2, cardHeight / 2);

                gc.FillEllipse(Brushes.Purple,center.X, center.Y, 10,10);

                String symbol = getWertSymbol(wert);
                String unicodefarbe = getUnicodeFarbe(farbe);

                gc.DrawString(symbol, schriftart, schriftfarbe, center.X - 25, center.Y - 45);
                gc.DrawString(symbol, schriftart, schriftfarbe, center.X + 10, center.Y - 45);

                gc.DrawString(unicodefarbe, schriftart, schriftfarbe, center.X - 24, center.Y - 25);
                gc.DrawString(unicodefarbe, schriftart, schriftfarbe, center.X + 12, center.Y - 25);

                gc.RotateTransform(180);

                center = new Point(-cardWidth / 2, -cardHeight / 2);

                gc.DrawString(symbol, schriftart, schriftfarbe, center.X - 25, center.Y - 45);
                gc.DrawString(symbol, schriftart, schriftfarbe, center.X + 10, center.Y - 45);

                gc.DrawString(unicodefarbe, schriftart, schriftfarbe, center.X - 24, center.Y - 25);
                gc.DrawString(unicodefarbe, schriftart, schriftfarbe, center.X + 12, center.Y - 25);

                gc.RotateTransform(180);
            }
            else
            {
                Brush brush = new System.Drawing.Drawing2D.HatchBrush(System.Drawing.Drawing2D.HatchStyle.DashedHorizontal, Color.DarkGreen);
                gc.FillRectangle(brush, 0, 0, cardWidth, cardHeight);
            }
        }

        public static String getUnicodeFarbe(Kartenfarbe farbe)
        {
            switch (farbe)
            {
                case Kartenfarbe.Kreuz: return "\u2663 ";
                case Kartenfarbe.Pik: return "\u2660";
                case Kartenfarbe.Herz: return "\u2665";
                case Kartenfarbe.Karo: return "\u2666";
                default: return " ";
            }
        }

        public static String getWertSymbol(Kartenwert wert)
        {
                switch (wert)
                {
                    case Kartenwert.Sieben: return "7";

                    case Kartenwert.Acht: return "8";

                    case Kartenwert.Neun: return "9";

                    case Kartenwert.Zehn: return "10";

                    case Kartenwert.Bube: return "B";

                    case Kartenwert.Dame: return "D";

                    case Kartenwert.Koenig: return "K";

                    case Kartenwert.Ass: return "A";

                    default:
                        return " ";
                }
        }
    }
}
