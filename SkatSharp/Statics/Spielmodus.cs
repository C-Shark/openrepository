﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkatSharp
{
    /// <summary>
    /// Die drei möglichen Spielarten
    /// </summary>
    public enum Spielart
    {
        Farbspiel,
        Null,
        Grand
    }

    /// <summary>
    /// Die drei Spielvarianten zu den Spielarten, die den evtl. Spielwert erhöhen
    /// </summary>
    public enum Spielmodus
    {
        Normal,
        Hand,
        Ouvert,
        OuvertHand
    }
}
