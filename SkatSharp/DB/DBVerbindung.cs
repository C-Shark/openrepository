﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Data;

namespace SkatSharp.DB
{
    class DBVerbindung : IDisposable
    {
        private OleDbConnection verbindung;
        private OleDbCommand sqlBefehl;
        private OleDbDataAdapter sqlAdapter;
        private string connectionstring;

        public void Dispose()
        {
            
        }

        /// <summary>
        /// Datenbankverbindung herstellen <br />
        /// Wirft OleDbException
        /// </summary>
        /// <param name="dateiname"></param>
        internal void connect(string dateiname)
        {
            connectionstring = "Provider=Microsoft.JET.OLEDB.4.0;Data Source=" + Application.StartupPath+ dateiname + " ;Persist Security Info=False";            

            try
            {
                verbindung = new OleDbConnection(connectionstring);
                verbindung.Open();
            }
            catch (OleDbException oe)
            {
                MessageBox.Show(oe.Message);                
            }

            sqlBefehl = new OleDbCommand();
            sqlBefehl.Connection = verbindung;

            sqlAdapter = new OleDbDataAdapter();
        }

        /// <summary>
        /// Gibt DataTable mit den Spielernamen und Passwörtern zurück
        /// </summary>
        /// <param name="dateiname"></param>
        /// <returns>DataTable Spieler(Pseudonym,Passwort)</returns>
        internal DataTable getSpielerListe(string dateiname)
        {
            connect(dateiname);

            DataTable spieler = new DataTable("Spieler");

            string selectcmd = "SELECT Pseudonym, Passwort FROM Spieler";

            sqlBefehl.CommandText = selectcmd;
            sqlAdapter.SelectCommand = sqlBefehl;

            sqlAdapter.Fill(spieler);
            verbindung.Close();
            return spieler;
        }

        /// <summary>
        /// Gibt DataTable der Spiele zurück, bei denen der angegebene Spieler Solospieler war
        /// </summary>
        /// <param name="dateiname"></param>
        /// <param name="pseudonym"></param>
        /// <returns>DataTable Spiele(Solospieler=Pseudonym)</returns>
        internal DataTable getSoloSpiele(String dateiname, String pseudonym)
        {
            connect(dateiname);

            String neuPseudonym = cleanforSQLString(pseudonym);

            DataTable spiele = new DataTable("Spiele");

            string selectcmd = "SELECT * FROM Spiele WHERE Solospieler = '" + neuPseudonym + "'";

            sqlBefehl.CommandText = selectcmd;
            sqlAdapter.SelectCommand = sqlBefehl;

            sqlAdapter.Fill(spiele);
            verbindung.Close();
            return spiele;

        }

        /// <summary>
        /// Aktualisiert die Spielertabelle, indem alle Spiele gezählt werden, an denen der Spieler beteiligt war
        /// </summary>
        /// <param name="dateiname"></param>
        /// <param name="pseudonym"></param>
        internal void updateSpielAnzahl(String dateiname, String pseudonym)
        {
            connect(dateiname);

            string selectcmd = "SELECT COUNT (*) FROM Spiele"
             + " WHERE Solospieler = '" + cleanforSQLString(pseudonym) + "'"
             + " OR Gegenspieler1 = '" + cleanforSQLString(pseudonym) + "'"
             + "OR Gegenspieler2 = '" + cleanforSQLString(pseudonym) + "'";
            sqlBefehl.CommandText = selectcmd;
            OleDbDataReader reader = sqlBefehl.ExecuteReader();

            reader.Read();
            int x = reader.GetInt32(0);
            reader.Close();

            string upcmd = "UPDATE Spieler SET Spiele= " + x + " WHERE Pseudonym = '" + cleanforSQLString(pseudonym) + "';";
            sqlBefehl.CommandText = upcmd;
            try
            {
                sqlBefehl.ExecuteNonQuery();
            }
            catch (OleDbException oe)
            {
                MessageBox.Show(oe.Message);
            }
            trennen();
        }

        /// <summary>
        /// Gibt alle Spiele als DataTable zurück, an denen der Spieler beteiligt war
        /// </summary>
        /// <param name="dateiname"></param>
        /// <param name="pseudonym"></param>
        /// <returns>DataTable Spiele(Contains:Pseudonym)</returns>
        internal DataTable getStatistik(String dateiname, String pseudonym)
        {
            connect(dateiname);

            DataTable spiele = new DataTable("Spiele");

            string selectcmd = "SELECT * FROM Spiele"
            + " WHERE Solospieler = '" + cleanforSQLString(pseudonym) + "'"
            + " OR Gegenspieler1 = '" + cleanforSQLString(pseudonym) + "'"
            + "OR Gegenspieler2 = '" + cleanforSQLString(pseudonym) + "'";

            sqlBefehl.CommandText = selectcmd;

            sqlAdapter.SelectCommand = sqlBefehl;

            sqlAdapter.Fill(spiele);

            trennen();

            return spiele;            
        }

        /// <summary>
        /// Entfernt Apostrophs sowie Semikolons
        /// </summary>
        /// <param name="s"></param>
        /// <returns>Clean String</returns>
        private String cleanforSQLString(String s)
        {
            String newString = s;
            newString = newString.Replace("'", "");
            newString = newString.Replace(";", "");

            return newString;
        }

        /// <summary>
        /// Der gegebene Spieler wird der Spieler-DB hinzugefügt, falls er nicht schon vorhanden ist
        /// </summary>
        /// <param name="dateiname"></param>
        /// <param name="pseudonym"></param>
        /// <param name="passwort"></param>
        internal void addToSpieler(String dateiname, String pseudonym, String passwort)
        {
            connect(dateiname);

            if (String.IsNullOrEmpty(pseudonym))
            {
                //new OleDbException();
                throw new Exception("Name ist leer!");
            }
            string testcmd = "SELECT Pseudonym FROM Spieler WHERE Pseudonym = '" + cleanforSQLString(pseudonym) + "';";
            sqlBefehl.CommandText = testcmd;
            OleDbDataReader reader = sqlBefehl.ExecuteReader();

            if (!reader.HasRows)
            {
                reader.Close();
                string insertcmd = "INSERT INTO Spieler (Pseudonym, Passwort)"
                    + "VALUES ('" + cleanforSQLString(pseudonym) + "',' " + cleanforSQLString(passwort) + "');";

                sqlBefehl.CommandText = insertcmd;
                sqlBefehl.ExecuteNonQuery();
            }
            
            trennen();
        }

        /// <summary>
        /// Ändert das Passwort eines Spielers
        /// </summary>
        /// <param name="dateiname"></param>
        /// <param name="pseudonym"></param>
        /// <param name="passwort"></param>
        internal void updatePW(String dateiname, String pseudonym, String passwort)
        {
            connect(dateiname);

            if (String.IsNullOrEmpty(pseudonym))
            {
                throw new Exception("Name ist leer!");
            }

            string updatecmd = "UPDATE Spieler SET Passwort= '" + cleanforSQLString(passwort) + "' WHERE Pseudonym = '" + cleanforSQLString(pseudonym) + "'";
            sqlBefehl.CommandText = updatecmd;

            try
            {
                sqlBefehl.ExecuteNonQuery();
            }
            catch (Exception)
            {
                
                throw;
            }

            trennen();
        }

        /// <summary>
        /// Beenden der Verbindung <br />
        /// Wirft OleDbException
        /// </summary>
        private void trennen()
        {
            try
            {

                verbindung.Close();
            }
            catch (OleDbException oe)
            {
                throw oe;
                //MessageBox.Show(oe.Message);
            }
        }

        /// <summary>
        /// Erzeugt die temporäre Datenbanktabelle der Spiele <br />
        /// Falls bereits vorhanden, wird diese geleert
        /// </summary>
        /// <param name="dateiname"></param>
        internal void createRundentabelle(String dateiname)
        {
            connect(dateiname);

            try
            {
                sqlBefehl.CommandText = "DELETE * FROM Rundentabelle";
                sqlBefehl.ExecuteNonQuery();
            }
            catch (OleDbException)
            {
                sqlBefehl.CommandText = "CREATE TABLE Rundentabelle "
                        + "( \n"
                    + "Solospieler CHARACTER, \n"
                    + "Gegenspieler1 CHARACTER, \n"
                    + "Gegenspieler2 CHARACTER ,\n"
                    + "Spielart CHARACTER, \n"
                    + "Spielmodus CHARACTER,\n "
                    + "Farbe CHARACTER, \n"
                    + "Reizwert INTEGER, \n"
                    + "Spielwert  INTEGER,\n "
                    + " Gewonnen BIT, \n"
                    + " Punkte_Solo INTEGER, \n"
                    + " punkte_Alten INTEGER,\n "
                    + "Ausgepasst BIT \n"
                        + " );";

                sqlBefehl.ExecuteNonQuery();
            }

            trennen();
        }

        /// <summary>
        /// Summiert die erworbenen Punkte und gibt sie als Tabelle zurück
        /// </summary>
        /// <param name="dateiname"></param>
        /// <returns>DataTable (Points)</returns>
        internal DataTable getHighscore(String dateiname)
        {
            connect(dateiname);

            DataTable spiele = new DataTable("Spiele");
            DataTable highscore = new DataTable("Highscore");

            highscore.Columns.Add("Spieler", typeof(String));
            highscore.Columns.Add("Punkte", typeof(Int32));

            String selectcmd = "SELECT Solospieler FROM Rundentabelle"
            + " UNION SELECT Gegenspieler1 FROM Rundentabelle"
            + " UNION SELECT Gegenspieler2 FROM Rundentabelle";

            sqlBefehl.CommandText = selectcmd;

            OleDbDataReader reader = sqlBefehl.ExecuteReader();

            while (reader.Read())
            {
                highscore.Rows.Add(reader.GetString(0), 0);
            }

            reader.Close();

            selectcmd = "SELECT * FROM Rundentabelle";

            sqlBefehl.CommandText = selectcmd;

            sqlAdapter.SelectCommand = sqlBefehl;

            sqlAdapter.Fill(spiele);
            if (spiele.Rows.Count > 0)
            {
                for (int i = 0; i < 3; i++)
                {
                    String sp = highscore.Rows[i][0].ToString(); //"Solospieler = '" + sp + "'"
                    if (spiele.Select("Solospieler = '" + sp + "' and gewonnen = true").Count() > 0)
                    {
                        highscore.Rows[i]["Punkte"] = Convert.ToInt32(spiele.Compute("Sum(Spielwert)", "Solospieler = '" + sp + "' and gewonnen = true"));
                    }
                    else if (spiele.Select("Solospieler = '" + sp + "' and gewonnen = false").Count() > 0)
                    {
                        highscore.Rows[i]["Punkte"] = Convert.ToInt32(spiele.Compute("-2*Sum(Spielwert)", "Solospieler = '" + sp + "' and gewonnen = false"));
                    }
                }
            }

            trennen();

            return highscore;
        }

        /// <summary>
        /// Die Spiele der temporären Liste werden der Datenbank aller Spiele hinzugefügt
        /// </summary>
        /// <param name="dateiname"></param>
        internal void joinTables(String dateiname)
        {
            connect(dateiname);

            sqlBefehl.CommandText = "INSERT INTO Spiele SELECT * FROM Rundentabelle";

            sqlBefehl.ExecuteNonQuery();

            sqlBefehl.CommandText = "DELETE * FROM Rundentabelle;";
            sqlBefehl.ExecuteNonQuery();

            trennen();
        }

        /// <summary>
        /// Ein Spiel mit all seinen Parametern wird der temporären Liste hinzugefügt
        /// </summary>
        /// <param name="dateiname"></param>
        /// <param name="solospieler"></param>
        /// <param name="gegenspieler1"></param>
        /// <param name="gegenspieler2"></param>
        /// <param name="spielart"></param>
        /// <param name="Spielmodus"></param>
        /// <param name="Farbe"></param>
        /// <param name="reizwert"></param>
        /// <param name="spielwert"></param>
        /// <param name="gewonnen"></param>
        /// <param name="punkte_s"></param>
        /// <param name="punkte_a"></param>
        /// <param name="ausgepasst"></param>
        internal void addToSpiele(String dateiname, String solospieler, String gegenspieler1,
            String gegenspieler2, string spielart, String Spielmodus, String Farbe,
            int reizwert, int spielwert, bool gewonnen, int punkte_s, int punkte_a, bool ausgepasst)
        {
            connect(dateiname);
            string insertcmd;

            if (ausgepasst)
            {
               insertcmd = "INSERT INTO Rundentabelle ("
                    + "Solospieler , Gegenspieler1,"
                 + "Gegenspieler2 , Spielart , Spielmodus , Farbe,"
                 + "Reizwert  ,Spielwert  ,Gewonnen  ,Punkte_Solo  ,punkte_Alten, Ausgepasst )"
                    + "VALUES ("
                    + "'" + solospieler + "','" + gegenspieler1 + "','" +
                 gegenspieler2 + "'," + "''" + "," + "''" + "," + "''" + "," +
                 "0" + "," + "0" + "," + "false" + "," + "0" + "," + "0"
                    + "," + ausgepasst + ")"; 
            }
            else if (spielart == Spielart.Null.ToString())
            {
                insertcmd = "INSERT INTO Rundentabelle ("
                     + "Solospieler , Gegenspieler1,"
                  + "Gegenspieler2 , Spielart , Spielmodus ,"
                  + "Reizwert  ,Spielwert  ,Gewonnen  , Ausgepasst )"
                     + "VALUES ("
                    + "'" + solospieler + "','" + gegenspieler1 + "','" +
                 gegenspieler2 + "','" + spielart + "','" + Spielmodus + "'," +
                 reizwert + "," + spielwert + "," + gewonnen + "," + ausgepasst + ")";
            }
            else if (spielart == Spielart.Farbspiel.ToString())
            {
                insertcmd = "INSERT INTO Rundentabelle ("
                    + "Solospieler , Gegenspieler1,"
                 + "Gegenspieler2 , Spielart , Spielmodus , Farbe,"
                 + "Reizwert  ,Spielwert  ,Gewonnen  ,Punkte_Solo  ,punkte_Alten, Ausgepasst )"
                    + "VALUES ("
                    + "'" + solospieler + "','" + gegenspieler1 + "','" +
                 gegenspieler2 + "','" + spielart + "','" + Spielmodus + "','" + Farbe + "'," +
                 reizwert + "," + spielwert + "," + gewonnen + "," + punkte_s + "," + punkte_a
                    + "," + ausgepasst + ")";
            }
            else
            {
                insertcmd = "INSERT INTO Rundentabelle ("
                                   + "Solospieler , Gegenspieler1,"
                                + "Gegenspieler2 , Spielart , Spielmodus , "
                                + "Reizwert  ,Spielwert  ,Gewonnen  ,Punkte_Solo  ,punkte_Alten, Ausgepasst )"
                                   + "VALUES ("
                                   + "'" + solospieler + "','" + gegenspieler1 + "','" +
                                gegenspieler2 + "','" + spielart + "','" + Spielmodus + "'," +
                                reizwert + "," + spielwert + "," + gewonnen + "," + punkte_s + "," + punkte_a
                                   + "," + ausgepasst + ")";
            }

            sqlBefehl.CommandText = insertcmd;

            try
            {
                sqlBefehl.ExecuteNonQuery();
                verbindung.Close();
            }
            catch (OleDbException oe)
            {
                throw oe;
                //MessageBox.Show(oe.Message);
            }
            
        }

        internal DBVerbindung()
        { }

        /// <summary>
        /// Entfernen eines Spielers und löschen aller Spiele, die diesen enthalten
        /// </summary>
        /// <param name="dateiname"></param>
        /// <param name="pseudonym"></param>
        internal void deleteSpieler(string dateiname, String pseudonym)
        {
            connect(dateiname);

            String delcmd = "DELETE * FROM Spiele \n"
                + "WHERE Solospieler = '" + pseudonym + "'"
                + " OR Gegenspieler1 = '" + pseudonym + "'"
                + " OR Gegenspieler2 = '" + pseudonym + "'";
                

            sqlBefehl.CommandText = delcmd;

            try
            {
                sqlBefehl.ExecuteNonQuery();
            }
            catch (OleDbException)
            {
                throw;
            }


            delcmd = "DELETE * FROM Spieler \n WHERE Pseudonym = '" + pseudonym + "'";

            sqlBefehl.CommandText = delcmd;

            try
            {
                sqlBefehl.ExecuteNonQuery();
            }
            catch (OleDbException)
            {
                throw;
            }

            trennen();
        }
    }
}
