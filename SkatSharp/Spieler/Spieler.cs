﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace SkatSharp
{
    /// <summary>
    /// Abstrakte Spielerklasse
    /// Enthält wichtige Methoden für Reizen und Spiele
    /// </summary>
    public abstract class Spieler
    {        
        public String name;
        
        protected Graphics gc;
        protected Panel panel;
        public event Action<Spieler, Spielkarte> OnCardChosen;

        #region Constructor
        protected Spieler(Panel panel, Action reizAction)
        {
            this.panel = panel;
            this.reizAction = reizAction;
            gc = panel.CreateGraphics();
        } 

        #endregion

        #region Reizen
        protected int reizWert;
        protected int spitze;

        public int Spitzenfaktor
        {
            get
            {
                return spitze;
            }
        }

        public bool abwerfen = false;

        public bool spielwillig = true;

        protected Action reizAction;

        protected void druecken(List<Spielkarte> karten)
        {
            if (karten.Count > 2)
            {
                throw new ArgumentOutOfRangeException(karten.ToString(), "Zu viele Karten im Skat!");
            }
            this.skat.AddRange(karten);
            hand.RemoveAll(k => karten.Contains(k));
        }

        public abstract void reizen();

        internal abstract void sagen(Spieler vorhand);

        internal abstract void fragen(Spieler sprecher);

        public void calcReizwert()
        {
            spielwillig = true;

            reizWert = (Reizen.getSpitzenfaktor(hand)+3)*(int)Kartenfarbe.Kreuz;
        }

        #endregion

        #region Spielvorbereitung
        internal void setTrumpf(Kartenfarbe trumpf)
        {
            foreach (Spielkarte k in hand)
            {
                if (k.Farbe == trumpf || k.Wert == Kartenwert.Bube)
                {
                    k.isTrumpf = true;
                }
                else
                {
                    k.isTrumpf = false;
                }
            }

            hand = Utility.sortiereNachFarbeTrumpf(hand);
        }

        internal void setTrumpfGrand()
        {
            foreach (Spielkarte k in hand)
            {
                if (k.Wert == Kartenwert.Bube)
                {
                    k.isTrumpf = true;
                }
                else
                {
                    k.isTrumpf = false;
                }
            }

            hand = Utility.sortiereNachFarbeTrumpf(hand);
        }

        internal void setTrumpfNull()
        {
            foreach (Spielkarte k in hand)
            {
                k.isTrumpf = false;
            }

            hand = Utility.sortiereNachFarbeTrumpf(hand);
        }

        internal bool gedrueckt()
        {
            if (hand.Count > 10)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        internal abstract void taufe(Action<Spielart, Spielmodus, Kartenfarbe> taufeAction);
        #endregion

        #region Spiellogik
        internal List<Spielkarte> hand = new List<Spielkarte>();
        internal List<Spielkarte> skat = new List<Spielkarte>();
        internal List<Spielkarte> erspielteKarten = new List<Spielkarte>();

        /// <summary>
        /// Löscht alle Karten des Spielers
        /// </summary>
        public void purgeCards()
        {
            hand.Clear();
            skat.Clear();
            erspielteKarten.Clear();
        }

        /// <summary>
        /// Gibt dem Spieler eine Karte.
        /// </summary>
        /// <param name="karte"></param>
        public void giveSpielkarte(Spielkarte karte)
        {
        	if (hand.Count >= 10) {
        		throw new ArgumentOutOfRangeException(karte.ToString(), "Zu viele Karten auf der Hand!");
        	}
        	hand.Add(karte);
        }

        /// <summary>
        /// Gibt die Karten an das Spiel zurück
        /// </summary>
        /// <returns></returns>
        internal List<Spielkarte> kartenAbgeben()
        {
            List<Spielkarte> tempList = new List<Spielkarte>();

            tempList.AddRange(hand);
            hand.Clear();
            tempList.AddRange(skat);
            skat.Clear();
            tempList.AddRange(erspielteKarten);
            erspielteKarten.Clear();

            return tempList;
        }

        /// <summary>
        /// Nach Gewinn des Stiches bekommt der Spieler diese Karten als wertende Karten gutgeschrieben
        /// </summary>
        /// <param name="karten"></param>
        public void giveErspielteKarten(List<Spielkarte> karten)
        {
            erspielteKarten.AddRange(karten);
            karten.Clear();
        }

        /// <summary>
        /// Summiert aus den erspielten Karten inklusive Skat die Punkte
        /// </summary>
        /// <returns></returns>
        public int sumPunkte()
        {
            int sum = 0;
            foreach (Spielkarte k in erspielteKarten)
            {
                sum += Punktwerte.getPunktwert(k);
            }

            foreach (Spielkarte k in skat)
            {
                sum += Punktwerte.getPunktwert(k);
            }

            return sum;
        }

        /// <summary>
        /// Der Spieler erhält den Skat (genau 2 Karten)
        /// </summary>
        /// <param name="skat"></param>
        public virtual void giveSkat(List<Spielkarte> skat)
        {
            if (skat.Count != 2)
            {
                throw new ArgumentOutOfRangeException(skat.ToString(), "Falsche Zahl Karten im Skat!");
            }
            this.hand.AddRange(skat);
            skat.Clear();
            hand = Utility.sortiereNachFarbeTrumpf(hand);
        }

        /// <summary>
        /// Bestimmt die nach Regeln spielbaren Karten; Falschspiel wird ausgeschlossen
        /// </summary>
        /// <param name="stich"></param>
        /// <returns></returns>
        protected List<Spielkarte> getMoeglicheKarten(List<Spielkarte> stich)
        {
            abwerfen = false;

            if (stich.Count == 0)
            {
                return hand;
            }
            else
            {

                List<Spielkarte> moeglicheKarten = new List<Spielkarte>();
                Spielkarte ersteKarte = stich[0];
                Kartenfarbe farbe = ersteKarte.Farbe;

                // Fehl bedienen
                if (!ersteKarte.isTrumpf)
                {
                    if (this.hasFarbe(farbe))
                    {
                        foreach (Spielkarte karte in hand)
                        {
                            if (karte.Farbe == ersteKarte.Farbe)
                            {
                                if (Spiel.AktuelleSpielart == Spielart.Null)
                                {
                                    moeglicheKarten.Add(karte);
                                }
                                else if (karte.Wert != Kartenwert.Bube)
                                {
                                    moeglicheKarten.Add(karte);
                                }
                            }
                        }
                    }
                    else // Einstechen                    
                    {
                        moeglicheKarten = hand;
                        abwerfen = true;
                    }
                }
                // Trumpf bedienen
                else if (ersteKarte.isTrumpf)
                {
                    foreach (Spielkarte karte in hand)
                    {
                        if (karte.isTrumpf)
                        {
                            moeglicheKarten.Add(karte);
                        }
                    }
                }

                // Abwerfen
                if (moeglicheKarten.Count == 0)
                {
                    abwerfen = true;
                    return hand;
                }
                return moeglicheKarten;
            }
        }

        /// <summary>
        /// Kurze Testmethode, ob eine Farbe in der Hand vorhande ist
        /// </summary>
        /// <param name="farbe"></param>
        /// <returns></returns>
        protected bool hasFarbe(Kartenfarbe farbe)
        {
            foreach (Spielkarte karte in hand)
            {
                if (karte.Farbe == farbe && karte.Wert != Kartenwert.Bube)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Abstrakte Wahlmethode für die Spieler
        /// <br>Benötigt den Delegaten für die Spielfortsetzung</br>
        /// </summary>
        /// <param name="stich"></param>
        /// <param name="proceedDel"></param>
        public abstract void chooseCard(List<Spielkarte> stich);

        /// <summary>
        /// Methode zum Feuern des vererbten Events
        /// </summary>
        /// <param name="sp"></param>
        /// <param name="choice"></param>
        protected virtual void FireOnCardchosen(Spieler sp, Spielkarte choice)
        {
            OnCardChosen(this, choice);
        }

        /// <summary>
        /// Errechnet den bestmögliche Spitzenfaktor
        /// </summary>
        /// <returns>Spitzenfaktor</returns>
        public int getSpitzenfaktor()
        {
            List<Spielkarte> tempList = new List<Spielkarte>();
            tempList.AddRange(hand);
            if (skat.Count > 0)
            {
                tempList.AddRange(this.skat);
            }
            spitze = Reizen.getSpitzenfaktor(tempList);
            return spitze;
        }

        /// <summary>
        /// Errechnet den Spitzenfaktor anhand einer gegebenen Farbe
        /// </summary>
        /// <param name="farbe"></param>
        /// <returns></returns>
        public int getSpitzenfaktor(Kartenfarbe farbe)
        {
            List<Spielkarte> tempList = new List<Spielkarte>();
            tempList.AddRange(hand);
            if (skat.Count > 0)
            {
                tempList.AddRange(this.skat);
            }
            spitze = Reizen.getSpitzenfaktor(tempList, farbe);
            return spitze;
        }
        #endregion

    }
}
