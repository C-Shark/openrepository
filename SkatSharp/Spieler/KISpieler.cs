﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;

namespace SkatSharp
{
    /// <summary>
    /// Computerspieler
    /// Entscheidet selbstständig wie hoch und ob spielt werden soll
    /// sowie welche Karten gespielt werden.
    /// </summary>
	public class KISpieler : Spieler
	{

        private int maxReiz;
        ToolTip tTip;
        private Kartenfarbe preferredColor;
        private Spielmodus preferredMode;
        private Spielart preferredType;

        private int bereitschaft;

        // Strategie
        enum Strategie
        {
            Farbspiel,
            Null
        }

        private Strategie strategie;

        #region Reizen
        /// <summary>
        /// Fragemethode beim Reizen
        /// Es wird dabei mitgeprüft ob Reizen für den Spieler noch möglich ist
        /// und dann wird der Hörer gefragt.
        /// Bei der KI wird auch ein Tooltip ausgegeben
        /// </summary>
        internal override void fragen(Spieler hoerer)
        {
            if (Spiel.aktuellerReizwert > maxReiz)
            {
                spielwillig = false;
                if (!Einstellungen.ohnepause) {
                	tTip.ToolTipTitle = "Weg!";
                tTip.Show(" ", panel, Einstellungen.kispeed / 2);
                Thread.Sleep(Einstellungen.kispeed);
                }
                
                reizAction();
            }
            else
            {
            	if (!Einstellungen.ohnepause) {
                tTip.ToolTipTitle = Spiel.aktuellerReizwert.ToString() + "?";
                tTip.Show(" ", panel, Einstellungen.kispeed/2);
                Thread.Sleep(Einstellungen.kispeed);
            	}
                hoerer.sagen(this);
            }
        }

        internal override void sagen(Spieler hoerer)
        {
            if (maxReiz >= Spiel.aktuellerReizwert)
            {
            	if (!Einstellungen.ohnepause) {
                tTip.ToolTipTitle = "Ja!";
                tTip.Show(" ", panel, Einstellungen.kispeed / 2);
                Thread.Sleep(Einstellungen.kispeed);
            	}
                reizAction();
            }
            else
            {
                spielwillig = false;
                if (!Einstellungen.ohnepause) {
                tTip.ToolTipTitle = "Weg!";
                tTip.Show(" ", panel, Einstellungen.kispeed / 2);
                Thread.Sleep(Einstellungen.kispeed);
                }
                reizAction();
            }
        }

        /// <summary>
        /// Reizmethode der KI
        /// Erst wird das Blatt bewertet und dann nach Bereitschaft die Spielwilligkeit entschieden
        /// </summary>
        public override void reizen()
        {
            if (blattBewerten() > bereitschaft)
            {
                spielwillig = true;
            }
            else
            {
                spielwillig = false;
            }
            maxReiz = Reizen.getReizWert(preferredMode, preferredType, preferredColor, Reizen.getSpitzenfaktor(hand, preferredColor));
        }

        private int blattBewerten()
        {
            int blattbewertung = 0;

            int kreuze = hand.FindAll(s => s.Farbe == Kartenfarbe.Kreuz && s.Wert != Kartenwert.Bube).Count;
            int piken = hand.FindAll(s => s.Farbe == Kartenfarbe.Pik && s.Wert != Kartenwert.Bube).Count;
            int herze = hand.FindAll(s => s.Farbe == Kartenfarbe.Herz && s.Wert != Kartenwert.Bube).Count;
            int karos = hand.FindAll(s => s.Farbe == Kartenfarbe.Karo && s.Wert != Kartenwert.Bube).Count;
            int buben = hand.FindAll(s => s.Wert == Kartenwert.Bube).Count;
            int asse = hand.FindAll(s => s.Wert == Kartenwert.Ass).Count;
            int zehnen = hand.FindAll(s => s.Wert == Kartenwert.Zehn).Count;

            int[] farben = { kreuze, piken, herze, karos };

            if (farben.Max() == kreuze)
            {
                preferredColor = Kartenfarbe.Kreuz;
            }
            else if (farben.Max() == piken)
            {
                preferredColor = Kartenfarbe.Pik;
            }
            else if (farben.Max() == herze)
            {
                preferredColor = Kartenfarbe.Herz;
            }
            else if (farben.Max() == karos)
            {
                preferredColor = Kartenfarbe.Karo;
            }

            blattbewertung += 20*farben.Where(i => i == 0).Count();


            foreach (Spielkarte karte in hand)
            {
                if (karte.Wert == Kartenwert.Bube)
                {
                    blattbewertung += 2 * (int)karte.Farbe - 10;
                }
                else if (karte.Wert == Kartenwert.Zehn)
                {
                    if (hand.Find(k => k.Wert == Kartenwert.Ass && k.Farbe == karte.Farbe) != null)
                    {
                        blattbewertung += 15;
                    }
                    else
                    {
                        blattbewertung -= 10;
                    }
                }
                else if (karte.Wert == Kartenwert.Acht || karte.Wert == Kartenwert.Sieben || karte.Wert == Kartenwert.Neun)
                {
                    if (karte.Farbe != preferredColor)
                    {
                        blattbewertung -= 5;
                    }
                    else
                    {
                        blattbewertung += 6;
                    }                          
                }
                else if (karte.Wert == Kartenwert.Dame || karte.Wert == Kartenwert.Koenig)
                {
                    if (karte.Farbe == preferredColor)
                    {
                        blattbewertung += (int)karte.Wert;
                    }
                    else if (hand.Find(k => k.Farbe == karte.Farbe && k.Wert == Kartenwert.Zehn) == null || hand.Find(k => k.Farbe == karte.Farbe && k.Wert == Kartenwert.Ass) == null)
                    {
                        blattbewertung -= (int)karte.Wert;
                    }
                }
            }

            blattbewertung += 16 * asse;

            if (farben.Max() >= 3)
            {
                blattbewertung += 2 * farben.Max();
            }

            // Grandspiel
            if (buben >= 2 && asse >= 2 && hand.Contains(hand.Find(s => s.Wert == Kartenwert.Bube && s.Farbe == Kartenfarbe.Kreuz)))
            {
                preferredType = Spielart.Grand;
            }

            // Nullspiel
            if (buben <= 2 && asse < 1 && zehnen < 2)
            {
                preferredType = Spielart.Null;
            }            

            if (blattbewertung > 90)
            {
                preferredMode = Spielmodus.Ouvert; 
            }
            else if (blattbewertung > 75)
            {
                preferredMode = Spielmodus.Hand;
            }
            else
            {
                preferredMode = Spielmodus.Normal;
            }


            return blattbewertung;
        }
        #endregion

        #region Spielvorbereitung
        public override void giveSkat(List<Spielkarte> skat)
        {
            if (skat.Count != 2)
            {
                throw new ArgumentOutOfRangeException(skat.ToString(), "Falsche Zahl Karten im Skat!");
            }

            if (preferredMode == Spielmodus.Hand || preferredMode == Spielmodus.OuvertHand)
            {
                this.skat.AddRange(skat);
                skat.Clear();
            }
            else
            {
                this.hand.AddRange(skat);
                skat.Clear();
                hand = Utility.sortiereNachFarbeTrumpf(hand);
            }
        }

        internal override void taufe(Action<Spielart, Spielmodus, Kartenfarbe> taufeAction)
        {
            int minReiz = Spiel.aktuellerReizwert;

            blattBewerten();           

            int kreuze = hand.FindAll(s => s.Farbe == Kartenfarbe.Kreuz && s.Wert != Kartenwert.Bube).Count;
            int piken = hand.FindAll(s => s.Farbe == Kartenfarbe.Pik && s.Wert != Kartenwert.Bube).Count;
            int herze = hand.FindAll(s => s.Farbe == Kartenfarbe.Herz && s.Wert != Kartenwert.Bube).Count;
            int karos = hand.FindAll(s => s.Farbe == Kartenfarbe.Karo && s.Wert != Kartenwert.Bube).Count;

            List<Spielkarte> dKarten = new List<Spielkarte>();

            int k = 0;

            foreach (Spielkarte karte in hand)
            {
                if (k == 2)
                {
                    break;
                }
                if (karte.Wert == Kartenwert.Ass || karte.Wert == Kartenwert.Bube || karte.Farbe == preferredColor)
                {
                    continue;
                }
                else
                {
                    if (hand.FindAll(v => v.Farbe == karte.Farbe && v.Wert != Kartenwert.Bube).Count <= 2)
                    {
                        dKarten.Add(karte); 
                    }
                }
            }

            Random r = new Random();   

            while (dKarten.Count < 2)
            {
                Spielkarte dKarte;
                dKarte = hand[r.Next(hand.Count - 1)];

                if (!dKarten.Contains(dKarte))
                {
                    dKarten.Add(dKarte);
                }               
            }

            while (dKarten.Count > 2)
            {
                dKarten.RemoveAt(0);
            }
            
            druecken(dKarten);            

            if (!Einstellungen.ohnepause) {
            tTip.ToolTipTitle = "Ich Spiele:";
            tTip.Show(preferredType.ToString() + " " + preferredMode.ToString() + " " + preferredColor.ToString(), panel, 3000);
            }

            taufeAction.Invoke(preferredType, preferredMode, preferredColor);
        }
        #endregion

        #region drawing


        void panel_Paint(object sender, PaintEventArgs e)
        {
            drawCards();
        }

        private void drawCards()
        {
            if (!Einstellungen.ohnepause)
            {
                //panel.Refresh();
                foreach (Spielkarte karte in hand)
                {

                    this.gc.FillRectangle(new System.Drawing.Drawing2D.HatchBrush(System.Drawing.Drawing2D.HatchStyle.DashedHorizontal, Color.DarkGreen),
                        5 + hand.IndexOf(karte) * 5, 5 + hand.IndexOf(karte) * 2, 20, 40);

                    if (Einstellungen.mogeln)
                    {
                        karte.drawUpside(gc);
                    }
                    else
                    {
                        karte.drawDownside(gc);
                    }
                    //gc.DrawRectangle(Pens.White, 5 + hand.IndexOf(karte) * 5, 5 + hand.IndexOf(karte) * 2, 20, 40);

                    //panel.Controls.Add(karte);
                }
            }
        }

        #endregion

        #region Spiellogik
        // <summary>
        // Auswahlmethode der KI
        // Es wird zwischen zei Strategien unterschieden:
        // Der Standardfarbspiel und der Nullstrategie
        // Beim Farbspiel spielt man hoch um Stiche zu bekommen 
        // und beim Nullspiel genau das Gegenteil
        // </summary>
        public override void chooseCard(List<Spielkarte> stich)
        {
            Random rand = new Random();

            // Holen der aktuellen Spielart
            if (Spiel.AktuelleSpielart == Spielart.Null)
            {
                strategie = Strategie.Null;
            }

            // Als erstes benötigt man die spielbaren Karten
            List<Spielkarte> moeglich = getMoeglicheKarten(stich);

            int buben = moeglich.FindAll(s => s.Wert == Kartenwert.Bube).Count;
            int asse = moeglich.FindAll(s => s.Wert == Kartenwert.Ass).Count;
            int zehnen = moeglich.FindAll(s => s.Wert == Kartenwert.Zehn).Count;

            Spielkarte choice = null;

            if (moeglich.Count == 0)
            {
                return;
                //throw new Exception("Nicht genug Karten!");
            }

            // Standardauswahl im Farbspiel
            if (strategie == Strategie.Farbspiel)
            {
                // Als erste Karte wird immer die höchstmögliche gespielt
                //
                if (stich.Count == 0)
                {
                    moeglich.Sort(Utility.vergleicheNachWert);
                    choice = moeglich[moeglich.Count-1];
                }
                    // Bei ein oder zwei Karten wird versucht einzustechen.
                    // falls das nicht möglich ist, wird möglichst niedrig gespielt
                else if (stich.Count == 2)
                {
                    if (Utility.bessereKarte(stich[1], stich[0], false))
                    {

                        List<Spielkarte> bessereKarten = moeglich.FindAll(k => Utility.bessereKarte(k, stich[1], abwerfen));
                        if (bessereKarten.Count == 0)
                        {
                            moeglich.Sort(Utility.vergleicheNachPunktWert);
                            choice = moeglich[0];
                        }
                        else
                        {
                            bessereKarten.Sort(Utility.vergleicheNachPunktWert);
                            choice = bessereKarten[0];
                        }

                    }                    
                }
                else
                {
                    List<Spielkarte> bessereKarten = moeglich.FindAll(k => Utility.bessereKarte(k, stich[0], abwerfen));
                    if (bessereKarten.Count == 0)
                    {
                        moeglich.Sort(Utility.vergleicheNachPunktWert);
                        choice = moeglich[0];
                    }
                    else
                    {
                        bessereKarten.Sort(Utility.vergleicheNachPunktWert);
                        choice = bessereKarten[0];
                    }
                }
            }
            else
            {
                moeglich.Sort(Utility.vergleicheNachWert);
                choice = moeglich[0];
            }
            // Wähle schlechteste Karte, die den Stich bringt

            // Falls immer noch keine Wahl getroffen werden konnte,
            // wird irgendeine Karte gewählt.
            if (choice == null)
            {
                int r = rand.Next(0, Math.Min(0, moeglich.Count - 1));
                choice = moeglich[r];
            }           
           

            hand.Remove(choice);

            // Auslösen des Events aus der Basisklasse
            FireOnCardchosen(this, choice);           
        }
        #endregion


        #region Constructor
        public KISpieler(Panel panel , Action reizAction, int bereitschaft) : base(panel, reizAction)
        {
            this.reizAction = reizAction;
            this.bereitschaft = bereitschaft;
            tTip = new ToolTip();
            tTip.IsBalloon = true;
            tTip.ToolTipIcon = ToolTipIcon.Info;
            tTip.ShowAlways = true;
            this.panel.Paint += panel_Paint;
        }
#endregion
	}
}
