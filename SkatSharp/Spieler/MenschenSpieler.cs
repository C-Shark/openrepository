﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SkatSharp
{
    class MenschenSpieler : Spieler
    {
        #region Variablen
        private Panel spielpanel;
        private ListBox listboxreizen;
        private Label labelTrumpf;
        private Label spielwert;
        private Label labelSpielart;
        private Button spielButton;
        private Button wegButton;
        private Button frageButton;
        private CheckBox handCheckBox;
        private CheckBox ouvertCheckBox;
        private BackgroundWorker backgroundworkerHand;

        private Action<Spieler, Spielkarte> proceedDel;
        private Action<Spielart, Spielmodus, Kartenfarbe> taufeAction;
        private Spieler herausforderer;
        private Spieler hoerer;

        private bool handspiel;
        private List<Spielkarte> moeglich;
        private List<Spielkarte> zuDruecken;
        private bool hatgefragt;
        #endregion

        #region Reizen
        /// <summary>
        /// Bereitet das Fragen vor, es werden Buttons erstellt
        /// </summary>
        /// <param name="hoerer"></param>
        internal override void fragen(Spieler hoerer)
        {
            if (reizWert < Spiel.aktuellerReizwert)
            {
                MessageBox.Show("Sie wurden ausgereizt bei: " + Spiel.aktuellerReizwert.ToString());
                spielwillig = false;
                reizAction();
            }
            else
            {
                this.hoerer = hoerer;

                if (frageButton != null)
                {
                    frageButton.Dispose();
                }

                frageButton = new Button();
                frageButton.Location = new Point(spielpanel.Width / 2, 5);
                frageButton.Text = Spiel.aktuellerReizwert.ToString();
                frageButton.Click += frageButton_Click;

                if (wegButton != null)
                {
                    wegButton.Dispose();
                }

                wegButton = new Button();
                wegButton.Location = new Point(spielpanel.Width / 2, 30);
                wegButton.Text = "Weg";
                wegButton.Click += wegButton_Click;

                spielpanel.Controls.Add(frageButton);
                spielpanel.Controls.Add(wegButton);

            }
        }

        /// <summary>
        /// Event zum Fragen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frageButton_Click(object sender, EventArgs e)
        {
            hatgefragt = true;
            spielwillig = true;
            hoerer.sagen(this);
        }

        /// <summary>
        /// Click-Event bei Spielunwilligkeit beim Reizen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void wegButton_Click(object sender, EventArgs e)
        {
            spielwillig = false;
            reizAction();
        }

        /// <summary>
        /// Bestätigung des Spielwillens beim Reizen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void spielButton_Click(object sender, EventArgs e)
        {
            if (Spiel.aktuellerReizwert != reizWert)
            {
                try
                {
                    listboxreizen.SelectedIndex = listboxreizen.Items.IndexOf(Spiel.aktuellerReizwert);
                }
                catch (Exception)
                {
                    listboxreizen.SelectedIndex = -1;
                }
            }
            hatgefragt = true;
            spielwillig = true;
            reizAction();
        }
       
        /// <summary>
        /// Frage ob man Alleinspieler sein möchte
        /// </summary>
        /// <returns></returns>
        internal bool letzteChance()
        {
            if (!hatgefragt)
            {
                String message = "Möchten Sie spielen?";
                String caption = "Letzte Chance!";

                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result = MessageBox.Show(message, caption, buttons);

                if (result == DialogResult.Yes)
                {
                    spielwillig = true;
                    return true;
                }
                else
                {
                    spielwillig = false;
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Beantwortung der Frage beim Reizen
        /// </summary>
        /// <param name="sprecher"></param>
        internal override void sagen(Spieler sprecher)
        {
            if (reizWert >= Spiel.aktuellerReizwert)
            {
                herausforderer = sprecher;

                //MessageBox.Show(Spiel.aktuellerReizwert.ToString() + "?");    

                spielButton = new Button();
                spielButton.Location = new Point(spielpanel.Width / 2, 5);
                spielButton.Text = "Ja/Weiter";
                spielButton.Click += spielButton_Click;

                wegButton = new Button();
                wegButton.Location = new Point(spielpanel.Width / 2, 30);
                wegButton.Text = "Weg";
                wegButton.Click += wegButton_Click;

                spielpanel.Controls.Add(spielButton);
                spielpanel.Controls.Add(wegButton);
            }
            else
            {
                MessageBox.Show("Sie wurden ausgereizt bei: " + Spiel.aktuellerReizwert.ToString());
                spielwillig = false;
                reizAction();
            }
        }

        public override void reizen()
        {
            spielpanel = new Panel();
            spielpanel.Location = new Point(3 * panel.Width / 4, 0);
            spielpanel.Width = panel.Width / 4;
            spielpanel.Height = panel.Height;
            hatgefragt = false;

            listboxreizen = new ListBox();
            listboxreizen.Location = new Point(0, 0);
            listboxreizen.Width = panel.Width / 8;
            listboxreizen.Height = panel.Height - 30;
            listboxreizen.Enabled = true;
            listboxreizen.SelectionMode = SelectionMode.One;
            foreach (int s in Reizen.werte)
            {
                listboxreizen.Items.Add(s);
                if (s == reizWert)
                {
                    break;
                }
            }
            listboxreizen.SelectedIndex = 0;

            spielpanel.Controls.Add(listboxreizen);


            panel.Controls.Add(spielpanel);

            panel.Paint += panel_Paint;
            //ReizForm reizform = new ReizForm(reizWert);
            //reizform.ShowDialog();
        }        
        #endregion

        #region Spielvorbereitung
        private void kartenZumDruecken()
        {
            zuDruecken = new List<Spielkarte>();
            foreach (Spielkarte karte in hand)
            {
                karte.Click += karte_DrueckChoice;
                karte.MouseHover += karte_MouseHoverDruecken;
                karte.MouseLeave += karte_MouseLeaveDruecken;
            }
        }

        private void kartenZumDrueckenDeactivate()
        {
            foreach (Spielkarte karte in hand)
            {
                karte.Click -= karte_DrueckChoice;
                karte.MouseHover -= karte_MouseHoverDruecken;
                karte.MouseLeave -= karte_MouseLeaveDruecken;
            }
            foreach (Spielkarte karte in skat)
            {
                karte.Click -= karte_DrueckChoice;
                karte.MouseHover -= karte_MouseHoverDruecken;
                karte.MouseLeave -= karte_MouseLeaveDruecken;
            }
        }

        private void karte_DrueckChoice(object sender, EventArgs e)
        {
            Spielkarte k = (Spielkarte)sender;

            if (zuDruecken.Contains(k))
            {
                k.MouseHover += karte_MouseHoverDruecken;
                k.MouseLeave += karte_MouseLeaveDruecken;
                zuDruecken.Remove(k);
                k.BackColor = k.backcolor;
            }
            else
            {
                if (zuDruecken.Count == 2)
                {
                    zuDruecken[0].BackColor = zuDruecken[0].backcolor;
                    zuDruecken.RemoveAt(0);
                }
                k.MouseHover -= karte_MouseHoverDruecken;
                k.MouseLeave -= karte_MouseLeaveDruecken;
                zuDruecken.Add(k);
                k.BackColor = Color.Purple;
            }
        }

        internal override void taufe(Action<Spielart, Spielmodus, Kartenfarbe> taufeAction)
        {
            this.taufeAction = taufeAction;

            spielpanel.Controls.Clear();
            panel.Controls.Clear();

            panel.Controls.Add(spielpanel);

            if (!handspiel)
            {
                kartenZumDruecken();
            }

            Label labelfrage = new Label();
            labelfrage.AutoSize = true;
            labelfrage.Text = "Welches Spiel wollen Sie spielen?";
            labelfrage.Location = new Point(5, 5);
            spielpanel.Controls.Add(labelfrage);

            foreach (Kartenfarbe farbe in Enum.GetValues(typeof(Kartenfarbe)))
            {
                Button farbbutton = new Button();
                farbbutton.Location = new Point((int)(farbe - 9) * 30 + 10, 30);
                farbbutton.Width = 30;
                farbbutton.Height = 30;
                farbbutton.Font = new Font("Arial", 16F);
                farbbutton.UseVisualStyleBackColor = true;
                farbbutton.Name = Enum.GetName(typeof(Kartenfarbe), farbe);
                farbbutton.Click += farbbutton_Click;
                farbbutton.MouseHover += farbbutton_MouseHover;
                switch (farbe)
                {
                    case Kartenfarbe.Kreuz: farbbutton.Text = "\u2663 ";
                        break;
                    case Kartenfarbe.Pik: farbbutton.Text = "\u2660";
                        break;
                    case Kartenfarbe.Herz: farbbutton.Text = "\u2665";
                        break;
                    case Kartenfarbe.Karo: farbbutton.Text = "\u2666";
                        break;
                    default:
                        break;
                }
                spielpanel.Controls.Add(farbbutton);
            }

            Button grandbutton = new Button();
            grandbutton.Location = new Point(70, 70);
            grandbutton.Width = 50;
            grandbutton.Text = "Grand";
            grandbutton.Name = "grand";
            grandbutton.Click += farbbutton_Click;
            grandbutton.MouseHover += farbbutton_MouseHover;

            Button nullbutton = new Button();
            nullbutton.Location = new Point(120, 70);
            nullbutton.Width = 40;
            nullbutton.Text = "Null";
            nullbutton.Name = "null";
            nullbutton.Click += farbbutton_Click;
            nullbutton.MouseHover += farbbutton_MouseHover;

            handCheckBox = new CheckBox();
            handCheckBox.Text = "Hand";
            handCheckBox.Enabled = false;
            handCheckBox.Checked = handspiel;
            handCheckBox.Location = new Point(5, 70);

            ouvertCheckBox = new CheckBox();
            ouvertCheckBox.Text = "ouvert";
            ouvertCheckBox.Checked = false;
            ouvertCheckBox.Location = new Point(5, 100);

            Label spielwertLabel = new Label();
            spielwertLabel.Text = "Spielwert: ";
            spielwertLabel.AutoSize = true;
            spielwertLabel.Location = new Point(5, panel.Height - 40);

            spielwert = new Label();
            spielwert.Text = " ";
            spielwert.AutoSize = true;
            spielwert.Location = new Point(60, panel.Height - 40);

            Label ReizwertLabel = new Label();
            ReizwertLabel.Text = "Reizwert: " + Spiel.aktuellerReizwert;
            ReizwertLabel.AutoSize = true;
            ReizwertLabel.Location = new Point(5, panel.Height - 20);

            spielpanel.Controls.Add(grandbutton);
            spielpanel.Controls.Add(handCheckBox);
            spielpanel.Controls.Add(ouvertCheckBox);
            spielpanel.Controls.Add(spielwertLabel);
            spielpanel.Controls.Add(ReizwertLabel);
            spielpanel.Controls.Add(spielwert);
            spielpanel.Controls.Add(nullbutton);

            spielpanel.ResumeLayout(false);
            spielpanel.PerformLayout();

            if (!backgroundworkerHand.IsBusy)
            {
                backgroundworkerHand.RunWorkerAsync();
            }
        }

        void farbbutton_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            if (!handspiel)
            {
                druecken(zuDruecken);

                foreach (Spielkarte karte in zuDruecken)
                {
                    panel.Controls.Remove(karte);
                }

                kartenZumDrueckenDeactivate();
            }

            Spielmodus modus;

            if (handCheckBox.Checked)
            {
                modus = Spielmodus.Hand;
                if (ouvertCheckBox.Checked)
                {
                    modus = Spielmodus.OuvertHand;
                }
            }
            else if (ouvertCheckBox.Checked)
            {
                modus = Spielmodus.Ouvert;
            }
            else
            {
                modus = Spielmodus.Normal;
            }

            switch (b.Name)
            {
                case "Kreuz": taufeAction(Spielart.Farbspiel, modus, Kartenfarbe.Kreuz);
                    break;
                case "Pik": taufeAction(Spielart.Farbspiel, modus, Kartenfarbe.Pik);
                    break;
                case "Herz": taufeAction(Spielart.Farbspiel, modus, Kartenfarbe.Herz);
                    break;
                case "Karo": taufeAction(Spielart.Farbspiel, modus, Kartenfarbe.Karo);
                    break;
                case "grand": taufeAction(Spielart.Grand, modus, Kartenfarbe.Kreuz);
                    break;
                case "null": taufeAction(Spielart.Null, modus, Kartenfarbe.Kreuz);
                    break;
                default:
                    break;
            }

            spielpanel.Controls.Clear();
        }

        void farbbutton_MouseHover(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            Spielmodus modus;

            if (handCheckBox.Checked)
            {
                modus = Spielmodus.Hand;
                if (ouvertCheckBox.Checked)
                {
                    modus = Spielmodus.OuvertHand;
                }
            }
            else if (ouvertCheckBox.Checked)
            {
                modus = Spielmodus.Ouvert;
            }
            else
            {
                modus = Spielmodus.Normal;
            }

            switch (b.Name)
            {
                case "Kreuz": spielwert.Text = Reizen.getReizWert(modus, Spielart.Farbspiel, Kartenfarbe.Kreuz, getSpitzenfaktor(Kartenfarbe.Kreuz) + 1).ToString();
                    break;
                case "Pik": spielwert.Text = Reizen.getReizWert(modus, Spielart.Farbspiel, Kartenfarbe.Pik, getSpitzenfaktor(Kartenfarbe.Pik) + 1).ToString();
                    break;
                case "Herz": spielwert.Text = Reizen.getReizWert(modus, Spielart.Farbspiel, Kartenfarbe.Herz, getSpitzenfaktor(Kartenfarbe.Herz) + 1).ToString();
                    break;
                case "Karo": spielwert.Text = Reizen.getReizWert(modus, Spielart.Farbspiel, Kartenfarbe.Karo, getSpitzenfaktor(Kartenfarbe.Karo) + 1).ToString();
                    break;
                case "grand": spielwert.Text = Reizen.getReizWert(modus, Spielart.Grand, Kartenfarbe.Kreuz, getSpitzenfaktor() + 1).ToString();
                    break;
                case "null": spielwert.Text = Reizen.getReizWert(modus, Spielart.Null, Kartenfarbe.Kreuz,0).ToString();
                    break;
                default:
                    break;
            }
        }

        void karte_MouseLeaveDruecken(object sender, EventArgs e)
        {
            Spielkarte s = (Spielkarte)sender;
            s.BackColor = s.backcolor;
        }

        void karte_MouseHoverDruecken(object sender, EventArgs e)
        {
            Spielkarte s = (Spielkarte)sender;
            s.BackColor = Color.Tomato;

        }
        #endregion

        #region Spiellogik
        /// <summary>
        /// Anzeigen der Spieleigenschaften
        /// </summary>
        /// <param name="art"></param>
        /// <param name="modus"></param>
        /// <param name="farbe"></param>
        public void spielanzeige(Spielart art, Spielmodus modus, Kartenfarbe farbe)
        {
            if (spielpanel.InvokeRequired)
            {
                Action<Spielart, Spielmodus, Kartenfarbe> anzeigeAction = new Action<Spielart, Spielmodus, Kartenfarbe>(spielanzeige);
                spielpanel.Invoke(anzeigeAction,  art, modus, farbe);
            }
            else
            {
                spielpanel.Controls.Clear();

                if (Spiel.AktuelleSpielart == Spielart.Farbspiel)
                {
                    labelTrumpf = new Label();
                    labelTrumpf.Text = "Trumpf: " + farbe.ToString();
                    labelTrumpf.Location = new Point(5, 25);
                    spielpanel.Controls.Add(labelTrumpf);
                }

                labelSpielart = new Label();
                labelSpielart.Text = "Spielart: " + art.ToString() + " " + modus.ToString();
                labelSpielart.Location = new Point(5, 50);
                
                spielpanel.Controls.Add(labelSpielart);
            }
        }

        /// <summary>
        /// Bekommt den Skat und es wird nach Handspiel gefragt
        /// </summary>
        /// <param name="skat"></param>
        public override void giveSkat(List<Spielkarte> skat)
        {
            if (skat == null)
            {
                throw new ArgumentException("Skat nicht vorhanden!");
            }
            if (skat.Count > 2)
            {
                throw new ArgumentOutOfRangeException(skat.ToString(), "Zu viele Karten im Skat!");
            }

            string handfrage = "Möchten Sie Hand spielen?";
            string caption = "Achtung!";

            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show(handfrage, caption, buttons);

            if (result == DialogResult.Yes)
            {
                this.skat.AddRange(skat);
                skat.Clear();
                handspiel = true;
            }
            else
            {
                handspiel = false;
                this.hand.AddRange(skat);
                skat.Clear();
                hand = Utility.sortiereNachFarbeTrumpf(hand);
            }
        }

        /// <summary>
        /// Wiederherstellen der normalen Kartenfarbe
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void karte_MouseLeave(object sender, EventArgs e)
        {
            Spielkarte s = (Spielkarte)sender;
            if (moeglich.Contains(s))
            {
                s.BackColor = s.backcolor;
            }
        }

        /// <summary>
        /// Einfärben der Karte, falls sie spielbar ist
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void karte_MouseHover(object sender, EventArgs e)
        {            
            Spielkarte s = (Spielkarte)sender;
            if (moeglich.Contains(s))
            {
                s.BackColor = Color.LawnGreen;
            }
        }

        /// <summary>
        /// Methode bei gewählter Karte, feuert das entsprechende Event der Basis-Klasse
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CardChosen(object sender, EventArgs e)
        {
            Spielkarte choice = (Spielkarte)sender;
            
            if (moeglich.Contains(choice))
            {
                deactivateCards();
                hand.Remove(choice);
                panel.Invalidate();
                //backgroundworkerHand.RunWorkerAsync();
                FireOnCardchosen(this, choice);
                //proceedDel(this, choice);
            }

        }

        /// <summary>
        /// Entfernt die Mouse-Events von den Karten
        /// </summary>
        private void deactivateCards()
        {
            foreach (Spielkarte karte in hand)
            {
                karte.Click -= new EventHandler(CardChosen);
                karte.MouseEnter -= karte_MouseHover;
                karte.MouseLeave -= karte_MouseLeave;
            }
        }

        /// <summary>
        /// Aktiviert die Karten zum Spielen (Mousehover+click)
        /// </summary>
        private void activateCards()
        {
            if (this.spielButton != null)
            {
                this.spielButton.Dispose();
            }
            if (this.wegButton != null)
            {
                this.wegButton.Dispose();
            }

            foreach (Spielkarte karte in hand)
            {
                karte.Click += new EventHandler(CardChosen);
                karte.MouseEnter += karte_MouseHover;
                karte.MouseLeave += karte_MouseLeave;
            }
        }

        /// <summary>
        /// Bereitet die Wahl der nächsten Karte vor
        /// </summary>
        /// <param name="stich"></param>
        public override void chooseCard(List<Spielkarte> stich)
        {
            spielanzeige(Spiel.AktuelleSpielart, Spiel.AktuellerSpielmodus, Spiel.Trumpf);

            activateCards();

            moeglich = getMoeglicheKarten(stich);
        }
        #endregion
       
        #region Constructor
        /// <summary>
        /// Konstruktor für den menschlichen Spieler
        /// , bekommt auch einen Action-Delegaten für das Reizen
        /// </summary>
        /// <param name="panel"></param>
        /// <param name="reizAction"></param>
        public MenschenSpieler(Panel panel, Action reizAction)
            : base(panel, reizAction)
        {
            this.reizAction = reizAction;
            backgroundworkerHand = new BackgroundWorker();
            backgroundworkerHand.DoWork += backgroundworkerHand_DoWork;
            name = Einstellungen.aktuellerSpieler;
        }
        #endregion 

        #region Drawing
        /// <summary>
        /// Aufruf des Backgroundworkers zum Zeichnen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void panel_Paint(object sender, PaintEventArgs e)
        {
            if (!backgroundworkerHand.IsBusy)
            {
                backgroundworkerHand.RunWorkerAsync();
            }
        }

        /// <summary>
        /// Arbeitsmethode des Backgroundworkers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void backgroundworkerHand_DoWork(object sender, DoWorkEventArgs e)
        {
            drawCards();
        }

        /// <summary>
        /// Zeichnet die Karten
        /// <br>Ruft einen Backgroundworker auf</br>
        /// </summary>
        private void drawCards()
        {
            if (Spiel.running)
            {

                hand = Utility.sortiereNachFarbeTrumpf(hand);

                foreach (Control innercontrol in panel.Controls)
                {
                    if (innercontrol.GetType() == typeof(Spielkarte))
                    {
                        panelcontrolsRemove(innercontrol);
                    }
                }
                setCards();
                //spielanzeige(Spiel.AktuelleSpielart, Spiel.AktuellerSpielmodus, Spiel.Trumpf);
            }
        }

        /// <summary>
        /// Löscht alle noch vorhanden Controls, die Karten sind und
        /// fügt alle Karten aus der Hand hinzu
        /// </summary>
        private void setCards()
        {
            if (panel.InvokeRequired)
            {
                Action scDel = new Action(setCards);
                panel.Invoke(scDel);
            }
            else
            {
                foreach (Spielkarte karte in hand)
                {
                    karte.BackColor = karte.backcolor;
                    //karte.Text = karte.getUnicodeFarbe + " " + karte.getUnicodeFarbe + "\n" + karte.Wert.ToString();
                    karte.Location = new Point(5 + hand.IndexOf(karte) * (3 * panel.Width / 4) / hand.Count, 20);

                    panelControlsAdd(karte);
                    //panel.Refresh();
                }
            }
        }

        /// <summary>
        /// Hilfsmethode für den Backgroundworker, damit sein Prozess 
        /// auch die Forms verändern kann
        /// </summary>
        /// <param name="innercontrol"></param>
        private void panelcontrolsRemove(Control innercontrol)
        {
            if (panel.InvokeRequired)
            {
                Action<Spielkarte> pcrDel = new Action<Spielkarte>(panelcontrolsRemove);
                panel.Invoke(pcrDel, innercontrol);
            }
            else
            {
                panel.Controls.Remove(innercontrol);
            }
        }

        /// <summary>
        /// Hilfsmethode für den Backgroundworker, damit sein Prozess 
        /// auch die Forms verändern kann
        /// </summary>
        /// <param name="innercontrol"></param>
        private void panelControlsAdd(Spielkarte karte)
        {
            if (panel.InvokeRequired)
            {
                Action<Spielkarte> pcaDel = new Action<Spielkarte>(panelControlsAdd);
                panel.Invoke(pcaDel, karte);
            }
            else
            {
                panel.Controls.Add(karte);
            }
        }
        #endregion

    }


}
