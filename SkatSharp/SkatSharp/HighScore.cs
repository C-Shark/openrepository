﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SkatSharp.Forms
{
    /// <summary>
    /// Anzeige der Highscore nach dem Spiel (automatisch)
    /// </summary>
    public partial class HighScore : Form
    {
        DB.DBVerbindung db;
        Spiel dasSpiel;

        public HighScore(Spiel dasSpiel)
        {
            InitializeComponent();

            this.dasSpiel = dasSpiel;
            db = new DB.DBVerbindung();
            DataTable highscore = db.getHighscore(Einstellungen.dbPath);

            //DataTable tableGridView = new DataTable("Highscore");

            dataGridView1.DataSource = highscore;
        }

        private void save_Click(object sender, EventArgs e)
        {
            db.joinTables(Einstellungen.dbPath);
            dasSpiel.updateSpielerSpiele();
            this.Close();
        }

        private void dontsave_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
