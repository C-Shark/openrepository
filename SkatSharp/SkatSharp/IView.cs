﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkatSharp.Forms
{
    public interface IView
    {
        void drawCards(Spieler sp, List<Spielkarte> hand);
        void writeToConsole(String s);
        void clear();
        event Action<Spielkarte> OnCardChosen;
        void drawTaufButtons();
        void drawReizButtons();
        void activateCards(List<Spielkarte> moeglich);
        void activateDruecken();
        void clearSpieler();
        void drawSpiel();
    }
}
