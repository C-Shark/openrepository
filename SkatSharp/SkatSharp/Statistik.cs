﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SkatSharp
{
    /// <summary>
    /// Windowsfenster für die Spielerstatistiken
    /// </summary>
    public partial class Statistik : Form
    {
        private DataTable spieler;
        private String pseudonym;
        DB.DBVerbindung db;

        public Statistik()
        {
            InitializeComponent();

            db = new DB.DBVerbindung();

            spieler = db.getSpielerListe("\\data\\data.mdb");

            comboBoxSpieler.DataSource = spieler;
            comboBoxSpieler.DisplayMember = "Pseudonym";
            comboBoxSpieler.ValueMember = "Pseudonym";
            //comboBoxSpieler.SelectedIndex = -1;

            
            
        }

        private void updateData()
        {
            DataTable tabelle = db.getStatistik("\\Data\\Data.mdb", pseudonym);
            DataTable solospiele = db.getSoloSpiele("\\Data\\Data.mdb", pseudonym);

            int nichtAusgepassteSpieleZahl = tabelle.Select("Ausgepasst = false").Count();
            int nichtAusgepassteSoloSpieleZahl = solospiele.Select("Ausgepasst = false").Count();

            int spielezahl = tabelle.Select("1=1").Count();
            textBoxSpieleGesamt.Text = spielezahl.ToString();

            int solozahl = solospiele.Select("Solospieler = '" + pseudonym + "'").Count();
            textBoxSpieleSolo.Text = solozahl.ToString();

            int gewonnen = solospiele.Select("Gewonnen=True").Count();
            textBoxGewonnen.Text = gewonnen.ToString();

            // Anzahl der Spielfarben
            int kreuz = solospiele.Select("Farbe = 'Kreuz' ").Count();
            labelKreuz.Text = kreuz.ToString();

            int pik = solospiele.Select("Farbe = 'Pik'").Count();
            labelPik.Text = pik.ToString();

            int herz = solospiele.Select("Farbe = 'Herz'").Count();
            labelHerz.Text = herz.ToString();

            int karo = solospiele.Select("Farbe = 'Karo'").Count();
            labelKaro.Text = karo.ToString();


            int farbspiel = solospiele.Select("Spielart = 'Farbspiel'").Count();
            labelFarbspiele.Text = farbspiel.ToString();

            int grand = solospiele.Select("Spielart = 'Grand'").Count();
            labelGrand.Text = grand.ToString();

            int nullspiel = solospiele.Select("Spielart = 'Null'").Count();
            labelNull.Text = nullspiel.ToString();

            int hand = solospiele.Select("Spielmodus = 'Hand'").Count();
            labelHand.Text = hand.ToString();

            int ouvert = solospiele.Select("Spielmodus = 'Ouvert'").Count();
            labelOuvert.Text = ouvert.ToString();

            int ouverthand = solospiele.Select("Spielmodus = 'OuvertHand'").Count();
            labelOuvertHand.Text = ouverthand.ToString();

            if (spielezahl > 0)
            {
                double reizwert = Convert.ToDouble(tabelle.Compute("Sum(Reizwert)", "Ausgepasst = false")) / nichtAusgepassteSpieleZahl;
                labelReizwert.Text = reizwert.ToString();
            }

            if (solozahl > 0)
            {
                double spielwert = Convert.ToDouble(solospiele.Compute("Sum(Spielwert)", "Ausgepasst = false")) / nichtAusgepassteSoloSpieleZahl;
                labelSpielwert.Text = spielwert.ToString();

                double punkte = Convert.ToDouble(solospiele.Compute("Sum(Punkte_Solo)", "Ausgepasst = false")) / nichtAusgepassteSoloSpieleZahl;
                labelPunkte.Text = punkte.ToString();
            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBoxSpieler_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (cb.SelectedIndex != -1)
            {
                var rowView = cb.SelectedItem as DataRowView;                
                pseudonym = rowView["Pseudonym"].ToString();
                if (!String.IsNullOrEmpty(pseudonym))
                {
                    updateData();
                }
                
            }
        }
    }
}
