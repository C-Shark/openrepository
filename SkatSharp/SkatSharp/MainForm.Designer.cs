﻿namespace SkatSharp
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.spielToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.neuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.einstellungenMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spielBeendenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.programmBeendenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.benutzerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datenÄndernToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statistikAnzeigenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.kontoWechselnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kontoSchließenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kontoLöschenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hilfeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regelnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hilfeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelGegner1 = new System.Windows.Forms.Panel();
            this.panelGegner2 = new System.Windows.Forms.Panel();
            this.panelSpieler = new System.Windows.Forms.Panel();
            this.panelStiche = new System.Windows.Forms.Panel();
            this.RTB = new System.Windows.Forms.RichTextBox();
            this.buttonSpielen = new System.Windows.Forms.Button();
            this.buttonDrawNew = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelSpieler = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spielToolStripMenuItem,
            this.benutzerToolStripMenuItem,
            this.hilfeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(778, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // spielToolStripMenuItem
            // 
            this.spielToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.neuToolStripMenuItem,
            this.einstellungenMenuItem,
            this.spielBeendenToolStripMenuItem,
            this.programmBeendenToolStripMenuItem});
            this.spielToolStripMenuItem.Name = "spielToolStripMenuItem";
            this.spielToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.spielToolStripMenuItem.Text = "Spiel";
            // 
            // neuToolStripMenuItem
            // 
            this.neuToolStripMenuItem.Name = "neuToolStripMenuItem";
            this.neuToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.neuToolStripMenuItem.Text = "Neu";
            this.neuToolStripMenuItem.Click += new System.EventHandler(this.NeuToolStripMenuItemClick);
            // 
            // einstellungenMenuItem
            // 
            this.einstellungenMenuItem.Name = "einstellungenMenuItem";
            this.einstellungenMenuItem.Size = new System.Drawing.Size(180, 22);
            this.einstellungenMenuItem.Text = "Einstellungen";
            this.einstellungenMenuItem.Click += new System.EventHandler(this.einstellungenMenuItem_Click);
            // 
            // spielBeendenToolStripMenuItem
            // 
            this.spielBeendenToolStripMenuItem.Name = "spielBeendenToolStripMenuItem";
            this.spielBeendenToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.spielBeendenToolStripMenuItem.Text = "Spiel beenden";
            this.spielBeendenToolStripMenuItem.Click += new System.EventHandler(this.spielBeendenToolStripMenuItem_Click);
            // 
            // programmBeendenToolStripMenuItem
            // 
            this.programmBeendenToolStripMenuItem.Name = "programmBeendenToolStripMenuItem";
            this.programmBeendenToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.programmBeendenToolStripMenuItem.Text = "Programm beenden";
            this.programmBeendenToolStripMenuItem.Click += new System.EventHandler(this.programmBeendenToolStripMenuItem_Click);
            // 
            // benutzerToolStripMenuItem
            // 
            this.benutzerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.datenÄndernToolStripMenuItem,
            this.statistikAnzeigenToolStripMenuItem,
            this.toolStripSeparator1,
            this.kontoWechselnToolStripMenuItem,
            this.kontoSchließenToolStripMenuItem,
            this.kontoLöschenToolStripMenuItem});
            this.benutzerToolStripMenuItem.Name = "benutzerToolStripMenuItem";
            this.benutzerToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.benutzerToolStripMenuItem.Text = "Benutzer";
            // 
            // datenÄndernToolStripMenuItem
            // 
            this.datenÄndernToolStripMenuItem.Enabled = false;
            this.datenÄndernToolStripMenuItem.Name = "datenÄndernToolStripMenuItem";
            this.datenÄndernToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.datenÄndernToolStripMenuItem.Text = "Daten ändern";
            this.datenÄndernToolStripMenuItem.Click += new System.EventHandler(this.datenÄndernToolStripMenuItem_Click);
            // 
            // statistikAnzeigenToolStripMenuItem
            // 
            this.statistikAnzeigenToolStripMenuItem.Name = "statistikAnzeigenToolStripMenuItem";
            this.statistikAnzeigenToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.statistikAnzeigenToolStripMenuItem.Text = "Statistik anzeigen";
            this.statistikAnzeigenToolStripMenuItem.Click += new System.EventHandler(this.statistikAnzeigenToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(213, 6);
            // 
            // kontoWechselnToolStripMenuItem
            // 
            this.kontoWechselnToolStripMenuItem.Name = "kontoWechselnToolStripMenuItem";
            this.kontoWechselnToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.kontoWechselnToolStripMenuItem.Text = "Konto wechseln/anmelden";
            this.kontoWechselnToolStripMenuItem.Click += new System.EventHandler(this.kontoWechselnToolStripMenuItem_Click);
            // 
            // kontoSchließenToolStripMenuItem
            // 
            this.kontoSchließenToolStripMenuItem.Enabled = false;
            this.kontoSchließenToolStripMenuItem.Name = "kontoSchließenToolStripMenuItem";
            this.kontoSchließenToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.kontoSchließenToolStripMenuItem.Text = "Konto schließen";
            this.kontoSchließenToolStripMenuItem.Click += new System.EventHandler(this.kontoSchließenToolStripMenuItem_Click);
            // 
            // kontoLöschenToolStripMenuItem
            // 
            this.kontoLöschenToolStripMenuItem.Name = "kontoLöschenToolStripMenuItem";
            this.kontoLöschenToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.kontoLöschenToolStripMenuItem.Text = "Konto löschen";
            this.kontoLöschenToolStripMenuItem.Click += new System.EventHandler(this.kontoLöschenToolStripMenuItem_Click);
            // 
            // hilfeToolStripMenuItem
            // 
            this.hilfeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.regelnToolStripMenuItem,
            this.hilfeToolStripMenuItem1,
            this.aboutToolStripMenuItem});
            this.hilfeToolStripMenuItem.Name = "hilfeToolStripMenuItem";
            this.hilfeToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.hilfeToolStripMenuItem.Text = "Hilfe";
            // 
            // regelnToolStripMenuItem
            // 
            this.regelnToolStripMenuItem.Name = "regelnToolStripMenuItem";
            this.regelnToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.regelnToolStripMenuItem.Text = "Regeln";
            this.regelnToolStripMenuItem.Click += new System.EventHandler(this.regelnToolStripMenuItem_Click);
            // 
            // hilfeToolStripMenuItem1
            // 
            this.hilfeToolStripMenuItem1.Name = "hilfeToolStripMenuItem1";
            this.hilfeToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.hilfeToolStripMenuItem1.Text = "Hilfe";
            this.hilfeToolStripMenuItem1.Click += new System.EventHandler(this.hilfeToolStripMenuItem1_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // panelGegner1
            // 
            this.panelGegner1.Location = new System.Drawing.Point(27, 43);
            this.panelGegner1.Name = "panelGegner1";
            this.panelGegner1.Size = new System.Drawing.Size(181, 229);
            this.panelGegner1.TabIndex = 1;
            // 
            // panelGegner2
            // 
            this.panelGegner2.Location = new System.Drawing.Point(592, 43);
            this.panelGegner2.Name = "panelGegner2";
            this.panelGegner2.Size = new System.Drawing.Size(156, 229);
            this.panelGegner2.TabIndex = 2;
            // 
            // panelSpieler
            // 
            this.panelSpieler.Location = new System.Drawing.Point(8, 408);
            this.panelSpieler.Name = "panelSpieler";
            this.panelSpieler.Size = new System.Drawing.Size(760, 163);
            this.panelSpieler.TabIndex = 3;
            // 
            // panelStiche
            // 
            this.panelStiche.Location = new System.Drawing.Point(240, 96);
            this.panelStiche.Name = "panelStiche";
            this.panelStiche.Size = new System.Drawing.Size(300, 300);
            this.panelStiche.TabIndex = 4;
            // 
            // RTB
            // 
            this.RTB.Location = new System.Drawing.Point(800, 56);
            this.RTB.Name = "RTB";
            this.RTB.Size = new System.Drawing.Size(224, 480);
            this.RTB.TabIndex = 5;
            this.RTB.Text = "";
            // 
            // buttonSpielen
            // 
            this.buttonSpielen.Location = new System.Drawing.Point(48, 336);
            this.buttonSpielen.Name = "buttonSpielen";
            this.buttonSpielen.Size = new System.Drawing.Size(75, 23);
            this.buttonSpielen.TabIndex = 6;
            this.buttonSpielen.Text = "Weiter";
            this.buttonSpielen.UseVisualStyleBackColor = true;
            this.buttonSpielen.Visible = false;
            this.buttonSpielen.Click += new System.EventHandler(this.buttonSpielen_Click);
            // 
            // buttonDrawNew
            // 
            this.buttonDrawNew.Location = new System.Drawing.Point(48, 368);
            this.buttonDrawNew.Name = "buttonDrawNew";
            this.buttonDrawNew.Size = new System.Drawing.Size(75, 23);
            this.buttonDrawNew.TabIndex = 7;
            this.buttonDrawNew.Text = "Neumalen";
            this.buttonDrawNew.UseVisualStyleBackColor = true;
            this.buttonDrawNew.Visible = false;
            this.buttonDrawNew.Click += new System.EventHandler(this.buttonDrawNew_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelSpieler});
            this.statusStrip1.Location = new System.Drawing.Point(0, 577);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(778, 22);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabelSpieler
            // 
            this.toolStripStatusLabelSpieler.Name = "toolStripStatusLabelSpieler";
            this.toolStripStatusLabelSpieler.Size = new System.Drawing.Size(0, 17);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 599);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.buttonDrawNew);
            this.Controls.Add(this.buttonSpielen);
            this.Controls.Add(this.RTB);
            this.Controls.Add(this.panelStiche);
            this.Controls.Add(this.panelSpieler);
            this.Controls.Add(this.panelGegner2);
            this.Controls.Add(this.panelGegner1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.Text = "Skat♯";
            this.ResizeBegin += new System.EventHandler(this.MainForm_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        private System.Windows.Forms.Panel panelStiche;
        private System.Windows.Forms.Panel panelSpieler;
        private System.Windows.Forms.Panel panelGegner2;
        private System.Windows.Forms.Panel panelGegner1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hilfeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem regelnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hilfeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kontoSchließenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kontoWechselnToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem statistikAnzeigenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem datenÄndernToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem benutzerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem programmBeendenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spielBeendenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem neuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spielToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;

        #endregion
        private System.Windows.Forms.RichTextBox RTB;
        private System.Windows.Forms.ToolStripMenuItem einstellungenMenuItem;
        private System.Windows.Forms.Button buttonSpielen;
        private System.Windows.Forms.Button buttonDrawNew;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelSpieler;
        private System.Windows.Forms.ToolStripMenuItem kontoLöschenToolStripMenuItem;


    }
}

