﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SkatSharp.Forms
{
    /// <summary>
    /// Dialog für einen neuen Spieler
    /// Passwort muss Zeichen enthalten und
    /// die beiden Felder müssen übereinstimmen
    /// </summary>
    public partial class NeuerSpieler : Form
    {
        private string aktuellerSpieler;

        public NeuerSpieler()
        {
            InitializeComponent();
        }

        public NeuerSpieler(String aktuellerSpieler)
        {
            this.aktuellerSpieler = aktuellerSpieler;

            InitializeComponent();

            this.Text = "Passwort ändern";
            this.persistentTextBoxName.PersistentText = aktuellerSpieler;
            this.persistentTextBoxName.Enabled = false;
            this.buttonOK.Click -= buttonOK_Click;
            this.buttonOK.Click += buttonOK_Click_Update ;
        }

        private void buttonOK_Click_Update(object sender, EventArgs e)
        {
            DB.DBVerbindung db = new DB.DBVerbindung();
            DataTable spieler;

            try
            {
                spieler = db.getSpielerListe(Einstellungen.dbPath);
                if (spieler.Select("Pseudonym = '" + aktuellerSpieler + "'").Count() == 0)
                {
                    MessageBox.Show("Spieler nicht vorhanden!");
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            try
            {
                db.updatePW(Einstellungen.dbPath, aktuellerSpieler, persistentTextBoxPW1.Text.Trim());
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (OleDbException oe)
            {
                MessageBox.Show(oe.Message);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (persistentTextBoxName.Text == persistentTextBoxName.PersistentText)
            {
                 MessageBox.Show("Namen eingeben!");
                 return;
            }

            if (String.IsNullOrEmpty(persistentTextBoxPW1.Text)|| String.IsNullOrEmpty(persistentTextBoxPW2.Text))
            {
                 MessageBox.Show("Passwort eingeben!");
                 return;
            }
            if (persistentTextBoxPW1.Text != persistentTextBoxPW2.Text)
            {
                MessageBox.Show("Passwörter stimmen nicht überein!");
                return;
            }

            DB.DBVerbindung db = new DB.DBVerbindung();
            DataTable spieler;
            String neu = persistentTextBoxName.Text;
            try
            {
               spieler= db.getSpielerListe(Einstellungen.dbPath);
               if (spieler.Select("Pseudonym = '" + neu + "'").Count() > 0)
               {
                   MessageBox.Show("Spieler bereits vorhanden!");
                   return;
               }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            try
            {
                db.addToSpieler(Einstellungen.dbPath, neu, persistentTextBoxPW1.Text.Trim());
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (OleDbException oe)
            {
                MessageBox.Show(oe.Message);
            }
        }

        private void NeuerSpieler_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                if (aktuellerSpieler == null)
                {
                    buttonOK_Click(null, EventArgs.Empty);
                }
                else
                {
                    buttonOK_Click_Update(null, EventArgs.Empty);
                }
                
            }
        }
    }
}
