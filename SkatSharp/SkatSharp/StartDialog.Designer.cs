﻿namespace SkatSharp
{
    partial class StartDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownSpiele = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxGegner1 = new System.Windows.Forms.TextBox();
            this.textBoxGegner2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBoxHuman = new System.Windows.Forms.CheckBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.numericUpDownRunden = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.labelcomputername = new System.Windows.Forms.Label();
            this.textBoxComputer0 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpiele)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRunden)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(152, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Anzahl Spiele:";
            // 
            // numericUpDownSpiele
            // 
            this.numericUpDownSpiele.Location = new System.Drawing.Point(152, 56);
            this.numericUpDownSpiele.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numericUpDownSpiele.Name = "numericUpDownSpiele";
            this.numericUpDownSpiele.ReadOnly = true;
            this.numericUpDownSpiele.Size = new System.Drawing.Size(64, 20);
            this.numericUpDownSpiele.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Gegnername:";
            // 
            // textBoxGegner1
            // 
            this.textBoxGegner1.Location = new System.Drawing.Point(16, 120);
            this.textBoxGegner1.Name = "textBoxGegner1";
            this.textBoxGegner1.Size = new System.Drawing.Size(72, 20);
            this.textBoxGegner1.TabIndex = 3;
            this.textBoxGegner1.Text = "Bot_1";
            // 
            // textBoxGegner2
            // 
            this.textBoxGegner2.Location = new System.Drawing.Point(160, 120);
            this.textBoxGegner2.Name = "textBoxGegner2";
            this.textBoxGegner2.Size = new System.Drawing.Size(72, 20);
            this.textBoxGegner2.TabIndex = 5;
            this.textBoxGegner2.Text = "Bot_2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(160, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Gegnername:";
            // 
            // checkBoxHuman
            // 
            this.checkBoxHuman.AutoSize = true;
            this.checkBoxHuman.Checked = true;
            this.checkBoxHuman.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHuman.Location = new System.Drawing.Point(16, 160);
            this.checkBoxHuman.Name = "checkBoxHuman";
            this.checkBoxHuman.Size = new System.Drawing.Size(91, 17);
            this.checkBoxHuman.TabIndex = 6;
            this.checkBoxHuman.Text = "Selbst spielen";
            this.checkBoxHuman.UseVisualStyleBackColor = true;
            this.checkBoxHuman.CheckedChanged += new System.EventHandler(this.checkBoxHuman_CheckedChanged);
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(96, 216);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 7;
            this.buttonStart.Text = "Start!";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(184, 216);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(80, 24);
            this.buttonCancel.TabIndex = 8;
            this.buttonCancel.Text = "Abbrechen";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // numericUpDownRunden
            // 
            this.numericUpDownRunden.Enabled = false;
            this.numericUpDownRunden.Location = new System.Drawing.Point(16, 56);
            this.numericUpDownRunden.Name = "numericUpDownRunden";
            this.numericUpDownRunden.ReadOnly = true;
            this.numericUpDownRunden.Size = new System.Drawing.Size(64, 20);
            this.numericUpDownRunden.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Anzahl Runden:";
            // 
            // labelcomputername
            // 
            this.labelcomputername.AutoSize = true;
            this.labelcomputername.Location = new System.Drawing.Point(160, 152);
            this.labelcomputername.Name = "labelcomputername";
            this.labelcomputername.Size = new System.Drawing.Size(81, 13);
            this.labelcomputername.TabIndex = 11;
            this.labelcomputername.Text = "Computername:";
            this.labelcomputername.Visible = false;
            // 
            // textBoxComputer0
            // 
            this.textBoxComputer0.Location = new System.Drawing.Point(160, 176);
            this.textBoxComputer0.Name = "textBoxComputer0";
            this.textBoxComputer0.Size = new System.Drawing.Size(72, 20);
            this.textBoxComputer0.TabIndex = 12;
            this.textBoxComputer0.Text = "Bot_0";
            this.textBoxComputer0.Visible = false;
            // 
            // StartDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.ControlBox = false;
            this.Controls.Add(this.textBoxComputer0);
            this.Controls.Add(this.labelcomputername);
            this.Controls.Add(this.numericUpDownRunden);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.checkBoxHuman);
            this.Controls.Add(this.textBoxGegner2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxGegner1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericUpDownSpiele);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "StartDialog";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpiele)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRunden)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDownSpiele;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxGegner1;
        private System.Windows.Forms.TextBox textBoxGegner2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBoxHuman;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.NumericUpDown numericUpDownRunden;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelcomputername;
        private System.Windows.Forms.TextBox textBoxComputer0;
    }
}