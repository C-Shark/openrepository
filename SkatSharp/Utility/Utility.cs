﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace SkatSharp
{
    /// <summary>
    /// Sammlung von weiteren Hilfsfunktionen
    /// </summary>
	public static class Utility
	{
        /// <summary>
        /// Die (generische) Mischmethode
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="rnd"></param>
		public static void Shuffle<T>(this IList<T> list, Random rnd)
		{
		    for(var i=0; i < list.Count; i++)
		    {
		        list.Swap(i, rnd.Next(i, list.Count));
		    }
		}
		
        /// <summary>
        /// (Generische) Tauschmethode für ILists, bei Mischen verwendet
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="i"></param>
        /// <param name="j"></param>
		public static void Swap<T>(this IList<T> list, int i, int j)
		{
		    var temp = list[i];
		    list[i] = list[j];
		    list[j] = temp;
		}

        /// <summary>
        /// Sortiert nach Farbe und Trümpfen
        /// </summary>
        /// <param name="l"></param>
        /// <returns></returns>
        public static List<Spielkarte> sortiereNachFarbeTrumpf(List<Spielkarte> l)
        {
            l.Sort(vergleicheNachFarbeUndTrumpf);
            return l;
        }

        /// <summary>
        /// Vergleich nach Kartenreihenfolge 7-Ass
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int vergleicheNachWert(Spielkarte a, Spielkarte b)
        {
            if (a.Wert > b.Wert)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// Vergleich nach Augenwert 0-11
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int vergleicheNachPunktWert(Spielkarte a, Spielkarte b)
        {
            if (Punktwerte.getPunktwert(a) > Punktwerte.getPunktwert(b))
            {
                return 1;
            }
            else if (Punktwerte.getPunktwert(a) < Punktwerte.getPunktwert(b))
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Vergleich nach aktueller Spielstärke und Farbe
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int vergleicheNachFarbeUndTrumpf(Spielkarte a, Spielkarte b)
        {
            if (a.isTrumpf && b.isTrumpf)
            {
                if (a.Wert == Kartenwert.Bube && b.Wert == Kartenwert.Bube)
                {
                    if (a.Farbe > b.Farbe)
                    {
                        return 1;
                    }
                    else
                    {
                        return -1;
                    }
                }
                else if (a.Wert == Kartenwert.Bube && b.Wert != Kartenwert.Bube)
                {
                    return 1;
                }
                else if (a.Wert != Kartenwert.Bube && b.Wert == Kartenwert.Bube)
                {
                    return -1;
                }
                else
                {
                    if (a.Wert > b.Wert)
                    {
                        return 1;
                    }
                    else
                    {
                        return -1;
                    }
                }
            }
            else
            {
                if (a.Farbe > b.Farbe)
                {
                    return 1;
                }
                else if (b.Farbe > a.Farbe)
                {
                    return -1;
                }
                else if (a.Wert > b.Wert)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
        }        

        /// <summary>
        /// Wichtige Vergleichsfunktion für Stiche
        /// Achtet auf Bubenstärke sowie aktuellem Spiel inklusive Trumpf
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="abwerfen"></param>
        /// <returns></returns>
        public static bool bessereKarte(Spielkarte a, Spielkarte b, bool abwerfen)
        {            
            if (abwerfen)
            {
                if (!a.isTrumpf)
                {
                    return false;
                }                
            }

            if ((b.isTrumpf))
            {
                if (a.isTrumpf)
                {
                    if ((b.Wert == a.Wert && a.Farbe > b.Farbe)
                        || (a.Wert == Kartenwert.Bube && b.Wert != Kartenwert.Bube)
                        || (a.Wert != Kartenwert.Bube && b.Wert != Kartenwert.Bube && a.Wert > b.Wert))
                    {
                        return true;
                    }
                }
            }
            else
            {
                if (a.isTrumpf)
                {
                    return true;
                }
                else if (a.Wert > b.Wert)
                {
                    return true;
                }
            }
            return false;
        }
                
        
        }

	}
                     
   

