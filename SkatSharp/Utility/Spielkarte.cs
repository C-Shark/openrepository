﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace SkatSharp
{
    /// <summary>
    /// Klasse für die Karten
    /// Erbt von Control
    /// </summary>
	public class Spielkarte : Control
    {	
        /// <summary>
        /// Konstruktor mit benötigten Parametern
        /// Jede Karte sollte im Skat nur einmal vorkommen
        /// (Bei zB Doppelkopf ist dies anders)
        /// </summary>
        /// <param name="farbe"></param>
        /// <param name="wert"></param>
		public Spielkarte(Kartenfarbe farbe, Kartenwert wert)
        {
        	this.farbe = farbe;
        	this.wert = wert;

            this.Width = 50;
            this.Height = 90;                                

            backcolor = Color.White;
            this.BackColor = backcolor;

            switch (Farbe)
            {
                case Kartenfarbe.Kreuz: forecolor = Color.Black;
                    break;
                case Kartenfarbe.Pik: forecolor = Color.Green;
                    break;
                case Kartenfarbe.Herz: forecolor = Color.Red;
                    break;
                case Kartenfarbe.Karo: forecolor = Color.Orange;
                    break;
                default:
                    break;
            }

        }

        private bool trumpf;
        private Kartenfarbe farbe;
        private Kartenwert wert;
        private Graphics gc;
        public Color backcolor;
        public Color forecolor;

        public bool upside = true;

        public bool isTrumpf
        {
            get { return trumpf; }
            set { trumpf = value; }
        }

        public String getUnicodeFarbe
        {
            get
            {
                switch (farbe)
                {
                     case Kartenfarbe.Kreuz: return "\u2663 ";                        
                    case Kartenfarbe.Pik: return "\u2660";
                    case Kartenfarbe.Herz: return "\u2665";
                    case Kartenfarbe.Karo: return "\u2666";
                    default: return " ";
                }
            }
        }

        public String getWertSymbol
        {
            get
            {
                switch (wert)
                {
                    case Kartenwert.Sieben: return "7";

                    case Kartenwert.Acht: return "8";

                    case Kartenwert.Neun: return "9";

                    case Kartenwert.Zehn: return "10";

                    case Kartenwert.Bube: return "B";

                    case Kartenwert.Dame: return "D";

                    case Kartenwert.Koenig: return "K";

                    case Kartenwert.Ass: return "A";
                        
                    default:
                        return " ";
                }
            }
        }

        public Kartenfarbe Farbe
        {
            get
            {
                return farbe;
            }
        }

        public Kartenwert Wert
        {
            get
            {
                return wert;
            }
        }


        protected override void OnPaint(PaintEventArgs e)
        {
            gc = this.CreateGraphics();

            if (upside)
            {
                drawUpside(this.gc);
            }
            else
            {
                drawDownside(this.gc);
            }

        }

        internal void drawDownside(Graphics graphic)
        {
            graphic = this.CreateGraphics();

            graphic.FillRectangle(Brushes.Navy, 0, 0, Width, Height);
        }

        internal void drawUpside(Graphics graphic)
        {
            // Malen des Hintergrundes
            graphic.FillRectangle(new SolidBrush(BackColor), 0, 0, Width, Height);
            graphic.DrawRectangle(Pens.Wheat, 0, 0, Width, Height);
            
            // Malen der Symbole und Farbe
            SolidBrush schriftfarbe = new SolidBrush(forecolor);
            Font schriftart = new Font("Arial", Width/4);

            Point center = new Point(Width / 2, Height / 2);

            graphic.DrawString(getWertSymbol, schriftart, schriftfarbe, center.X - 25, center.Y - 45);
            graphic.DrawString(getWertSymbol, schriftart, schriftfarbe, center.X + 10, center.Y - 45);

            graphic.DrawString(getUnicodeFarbe, schriftart, schriftfarbe, center.X - 24, center.Y - 25);
            graphic.DrawString(getUnicodeFarbe, schriftart, schriftfarbe, center.X + 12, center.Y - 25);

            graphic.RotateTransform(180);

            center = new Point(-Width / 2, -Height / 2);

            graphic.DrawString(getWertSymbol, schriftart, schriftfarbe, center.X - 25, center.Y - 45);
            graphic.DrawString(getWertSymbol, schriftart, schriftfarbe, center.X + 10, center.Y - 45);

            graphic.DrawString(getUnicodeFarbe, schriftart, schriftfarbe, center.X - 24, center.Y - 25);
            graphic.DrawString(getUnicodeFarbe, schriftart, schriftfarbe, center.X + 12, center.Y - 25);

            graphic.RotateTransform(180);
        }
    }
}
