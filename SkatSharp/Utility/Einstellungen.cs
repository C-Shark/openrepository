﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkatSharp
{
    static class Einstellungen
    {
        internal static String aktuellerSpieler;
        internal static String gegner1name;
        internal static String gegner2name;
        internal static String computername;
        internal static bool humanplayer;
        internal static int maxRunden;
        internal static int maxSpiele;
        internal static int speed;//1000;
        internal static int kispeed;//1000;
        internal static bool ohnepause;//false;
        internal static int bereitschaft0;
        internal static int bereitschaft1;
        internal static int bereitschaft2;
        internal static bool mogeln = false;
        internal static String dbPath = "\\data\\data.mdb";
    }
}
