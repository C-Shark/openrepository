﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SkatSharp
{
    /// <summary>
    /// Statische Klasse, die Hilfsfunktionen und Werte für das Reizen enthält
    /// </summary>
	public static class Reizen
	{
        /// <summary>
        /// Liefert den nächsten Reizwert in der Folge
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        public static int getNextIntReizwert(int r)
        {
            List<int> reizliste = werte.ToList();

            int i = reizliste.FindIndex(ind => ind == r);

            return reizliste[i + 1];
        }

        internal static int[] werte = new int[] {
                  18,20,22,23,24,27,30,33,35,36,40,44,45,46,48,50,
                  54,55,59,60,63,66,70,72,77,80,81,84,88,90,96,99,
                  100,108,110,117,120,121,126,130,132,135,140,143,
                  144,150,153,154,156,160,162,165,168,170,171,176,
                  180,187,189,190,192,198,200,204,207,209,210,216,
                  220,228,240,264,999
            };            
		
        internal static int getSpitzenfaktor(List<Spielkarte> hand)
        {
            int[] spitze = {0,0,0,0};
            int i = 0;
            foreach (Kartenfarbe farbe in Enum.GetValues(typeof(Kartenfarbe)))
            {
                if (hand.Find(k => k.Wert == Kartenwert.Bube && k.Farbe == Kartenfarbe.Kreuz) != null)
                {
                    spitze[i]++;
                    if (hand.Find(k => k.Wert == Kartenwert.Bube && k.Farbe == Kartenfarbe.Pik) != null)
                    {
                        spitze[i]++;
                        if (hand.Find(k => k.Wert == Kartenwert.Bube && k.Farbe == Kartenfarbe.Herz) != null)
                        {
                            spitze[i]++;
                            if (hand.Find(k => k.Wert == Kartenwert.Bube && k.Farbe == Kartenfarbe.Karo) != null)
                            {
                                spitze[i]++;
                                if (hand.Find(k => k.Wert == Kartenwert.Ass && k.Farbe == farbe) != null)
                                {
                                    spitze[i]++;
                                    if (hand.Find(k => k.Wert == Kartenwert.Zehn && k.Farbe == farbe) != null)
                                    {
                                        spitze[i]++;
                                        if (hand.Find(k => k.Wert == Kartenwert.Koenig && k.Farbe == farbe) != null)
                                        {
                                            spitze[i]++;
                                            if (hand.Find(k => k.Wert == Kartenwert.Dame && k.Farbe == farbe) != null)
                                            {
                                                spitze[i]++;
                                                if (hand.Find(k => k.Wert == Kartenwert.Neun && k.Farbe == farbe) != null)
                                                {
                                                    spitze[i]++;
                                                    if (hand.Find(k => k.Wert == Kartenwert.Acht && k.Farbe == farbe) != null)
                                                    {
                                                        spitze[i]++;
                                                        if (hand.Find(k => k.Wert == Kartenwert.Sieben && k.Farbe == farbe) != null)
                                                        {
                                                            spitze[i]++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    spitze[i]++;
                    if (hand.Find(k => k.Wert == Kartenwert.Bube && k.Farbe == Kartenfarbe.Pik) == null)
                    {
                        spitze[i]++;
                        if (hand.Find(k => k.Wert == Kartenwert.Bube && k.Farbe == Kartenfarbe.Herz) == null)
                        {
                            spitze[i]++;
                            if (hand.Find(k => k.Wert == Kartenwert.Bube && k.Farbe == Kartenfarbe.Karo) == null)
                            {
                                spitze[i]++;
                                if (hand.Find(k => k.Wert == Kartenwert.Ass && k.Farbe == farbe) == null)
                                {
                                    spitze[i]++;
                                    if (hand.Find(k => k.Wert == Kartenwert.Zehn && k.Farbe == farbe) == null)
                                    {
                                        spitze[i]++;
                                        if (hand.Find(k => k.Wert == Kartenwert.Koenig && k.Farbe == farbe) == null)
                                        {
                                            spitze[i]++;
                                            if (hand.Find(k => k.Wert == Kartenwert.Dame && k.Farbe == farbe) == null)
                                            {
                                                spitze[i]++;
                                                if (hand.Find(k => k.Wert == Kartenwert.Neun && k.Farbe == farbe) == null)
                                                {
                                                    spitze[i]++;
                                                    if (hand.Find(k => k.Wert == Kartenwert.Acht && k.Farbe == farbe) == null)
                                                    {
                                                        spitze[i]++;
                                                        if (hand.Find(k => k.Wert == Kartenwert.Sieben && k.Farbe == farbe) == null)
                                                        {
                                                            spitze[i]++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                i++;
            }
            return spitze.Max();
        }


        internal static int getSpitzenfaktor(List<Spielkarte> hand, Kartenfarbe farbe)
        {
            int spitze = 0;
            if (hand.Find(k => k.Wert == Kartenwert.Bube && k.Farbe == Kartenfarbe.Kreuz) != null)
            {
                spitze++;
                if (hand.Find(k => k.Wert == Kartenwert.Bube && k.Farbe == Kartenfarbe.Pik) != null)
                {
                    spitze++;
                    if (hand.Find(k => k.Wert == Kartenwert.Bube && k.Farbe == Kartenfarbe.Herz) != null)
                    {
                        spitze++;
                        if (hand.Find(k => k.Wert == Kartenwert.Bube && k.Farbe == Kartenfarbe.Karo) != null)
                        {
                            spitze++;
                            if (hand.Find(k => k.Wert == Kartenwert.Ass && k.Farbe == farbe) != null)
                            {
                                spitze++;
                                if (hand.Find(k => k.Wert == Kartenwert.Zehn && k.Farbe == farbe) != null)
                                {
                                    spitze++;
                                    if (hand.Find(k => k.Wert == Kartenwert.Koenig && k.Farbe == farbe) != null)
                                    {
                                        spitze++;
                                        if (hand.Find(k => k.Wert == Kartenwert.Dame && k.Farbe == farbe) != null)
                                        {
                                            spitze++;
                                            if (hand.Find(k => k.Wert == Kartenwert.Neun && k.Farbe == farbe) != null)
                                            {
                                                spitze++;
                                                if (hand.Find(k => k.Wert == Kartenwert.Acht && k.Farbe == farbe) != null)
                                                {
                                                    spitze++;
                                                    if (hand.Find(k => k.Wert == Kartenwert.Sieben && k.Farbe == farbe) != null)
                                                    {
                                                        spitze++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                spitze++;
                if (hand.Find(k => k.Wert == Kartenwert.Bube && k.Farbe == Kartenfarbe.Pik) == null)
                {
                    spitze++;
                    if (hand.Find(k => k.Wert == Kartenwert.Bube && k.Farbe == Kartenfarbe.Herz) == null)
                    {
                        spitze++;
                        if (hand.Find(k => k.Wert == Kartenwert.Bube && k.Farbe == Kartenfarbe.Karo) == null)
                        {
                            spitze++;
                            if (hand.Find(k => k.Wert == Kartenwert.Ass && k.Farbe == farbe) == null)
                            {
                                spitze++;
                                if (hand.Find(k => k.Wert == Kartenwert.Zehn && k.Farbe == farbe) == null)
                                {
                                    spitze++;
                                    if (hand.Find(k => k.Wert == Kartenwert.Koenig && k.Farbe == farbe) == null)
                                    {
                                        spitze++;
                                        if (hand.Find(k => k.Wert == Kartenwert.Dame && k.Farbe == farbe) == null)
                                        {
                                            spitze++;
                                            if (hand.Find(k => k.Wert == Kartenwert.Neun && k.Farbe == farbe) == null)
                                            {
                                                spitze++;
                                                if (hand.Find(k => k.Wert == Kartenwert.Acht && k.Farbe == farbe) == null)
                                                {
                                                    spitze++;
                                                    if (hand.Find(k => k.Wert == Kartenwert.Sieben && k.Farbe == farbe) == null)
                                                    {
                                                        spitze++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            return spitze;
        }
		
		public static int getReizWert(Spielmodus spielmodus, Spielart spielart, Kartenfarbe farbe, int spitzenfaktor)
		{
            switch (spielart)
            {
                case Spielart.Farbspiel: return (int)farbe * (spitzenfaktor + (int)spielmodus);

                case Spielart.Null: if (spielmodus == Spielmodus.Normal)
                    {
                        return 23;
                    }
                    else if (spielmodus == Spielmodus.Hand)
                    {
                        return 35;
                    }
                    else if (spielmodus == Spielmodus.Ouvert)
                    {
                        return 46;
                    }
                    else if (spielmodus == Spielmodus.OuvertHand)
                    {
                        return 59;
                    }
                    return -1;

                case Spielart.Grand: return 24 * (spitzenfaktor + (int)spielmodus);

                default: return -1;
            }			
		}
	}
}
